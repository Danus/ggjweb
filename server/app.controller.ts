import { Controller, Get } from '@nestjs/common';
import { Connection } from 'typeorm';
import { User } from './src/user/user.entity';

@Controller()
export class AppController {
	constructor(private connection: Connection) {}

	@Get('api/speakers')
	async findAllSpeakers() {
		const userrepo = this.connection.getRepository(User);
		return await userrepo.find();
	}
}
