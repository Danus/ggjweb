import { Module } from '@nestjs/common';
import {User} from './user.entity';
import {UserService} from './user.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import { Log } from '../log/log.entity';

@Module({
	imports: [TypeOrmModule.forFeature([User, Log])],
	providers: [UserService],
	exports: [UserService],
})
export class UserModule {}
