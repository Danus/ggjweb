import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { LoginDto, RegisterDto } from '../auth/auth.dto';
import * as crypto from 'crypto';
import * as JWT from 'jwt-decode';
import { Log } from '../log/log.entity';

@Injectable()
export class UserService {
	@InjectRepository(User)
	private readonly userRepository: Repository<User>;

	@InjectRepository(Log)
	private readonly logRepository: Repository<Log>;

	private static sanitizeUser(user: User): User {
		const sanitizedUser = user;
		delete sanitizedUser.password;
		return sanitizedUser;
	}

	public async getUser(id: number, token: string) {
		token = token.slice(7);
		const jwtUser: any = JWT(token);
		id = Number(id);

		if (jwtUser.role === 'admin' || jwtUser.role === 'event manager') {
			return await this.userRepository.findOne(id);
		} else if (jwtUser.role === 'user' || jwtUser.role === 'accountant') {
			if (jwtUser.userId === id) {
				return await this.userRepository.findOne(id);
			} else {
				throw new HttpException(
					{
						status: HttpStatus.UNAUTHORIZED,
						error: 'Unauthorized!',
					},
					401,
				);
			}
		} else {
			throw new HttpException(
				{
					status: HttpStatus.FORBIDDEN,
					error: 'Invalid Token!',
				},
				403,
			);
		}
	}

	public async getNotInGroup(token) {
		token = token.slice(7);
		token = JWT(token);

		if (token.role === 'admin' || token.role === 'user' || token.role === 'event manager') {
			return await this.userRepository.find({
				where: [{ role: 'user' }],
				select: ['id', 'emailAddress', 'firstName', 'lastName'],
				relations: ['groups', 'groupsLeader'],
			});
		} else {
			throw new HttpException(
				{
					status: HttpStatus.FORBIDDEN,
					error: 'Invalid Token!',
				},
				403,
			);
		}
	}

	// function to add a new user to the table
	public async registerUser(userDto: RegisterDto) {
		const {
			emailAddress,
			firstName,
			lastName,
			password,
			diet,
			allergies,
			skillSet,
			hardware,
			sponsorEmailAgreed,
			paid,
		} = userDto;
		const user = new User();
		user.emailAddress = emailAddress;
		user.salt = crypto.randomBytes(32).toString('hex');
		user.password = crypto.pbkdf2Sync(password, user.salt, 10000, 100, 'sha512').toString('hex');
		user.firstName = firstName;
		user.lastName = lastName;
		user.diet = diet;
		user.allergies = allergies;
		user.skillSet = skillSet;
		user.hardware = hardware;
		user.sponsorEmailAgreed = sponsorEmailAgreed;
		user.role = 'user';
		user.paid = paid;
		await this.userRepository.save(user);
	}

	// this is a function to find a user by login
	public async findByLogin(userDTO: LoginDto): Promise<User> {
		const { emailAddress, password } = userDTO;
		const user = await this.userRepository.findOne({ emailAddress: emailAddress });
		if (!user) {
			throw new HttpException('Invalid Credentials', HttpStatus.UNAUTHORIZED);
		}
		const hash = crypto.pbkdf2Sync(password, user.salt, 10000, 100, 'sha512').toString('hex');
		if (hash !== user.password) {
			throw new HttpException('Invalid Credentials', HttpStatus.UNAUTHORIZED);
		}
		await this.userRepository.save(user);
		return UserService.sanitizeUser(user);
	}

	// function to change/update user profile
	public async updateProfile(requestBody, token) {
		token = token.slice(7);
		token = JWT(token);

		if (
			(token.role === 'user' && token.userId === requestBody.id) ||
			(token.role === 'accountant' && token.userId === requestBody.id) ||
			token.role === 'admin' ||
			token.role === 'event manager'
		) {
			await this.updateProfileUser(requestBody, token);
		} else {
			throw new HttpException('Invalid token', HttpStatus.UNAUTHORIZED);
		}
	}

	// help function to update the user
	private async updateProfileUser(requestBody, user) {
		const updatedUser = await this.userRepository.findOne(requestBody.id);
		const { password, diet, allergies, skillSet, hardware, role, paid } = requestBody;
		const logObject = { user: user.emailAddress, updatedUser: updatedUser.emailAddress };

		if (password) {
			updatedUser.password = crypto
				.pbkdf2Sync(requestBody.password, updatedUser.salt, 10000, 100, 'sha512')
				.toString('hex');
		}
		if (diet) {
			logObject['diet'] = updatedUser.diet = diet;
		}
		if (allergies !== null) {
			logObject['allergies'] = updatedUser.allergies = allergies;
		}
		if (skillSet !== null) {
			logObject['skillSet'] = updatedUser.skillSet = skillSet;
		}
		if (hardware) {
			logObject['hardware'] = updatedUser.hardware = hardware;
		}
		if ((role && user.role === 'admin') || (role && user.role === 'event manager')) {
			logObject['role'] = updatedUser.role = role;
		}
		if ((paid && user.role === 'admin') || (paid && user.role === 'event manager')) {
			if (paid === 'true') {
				logObject['paid'] = updatedUser.paid = true;
			} else {
				logObject['paid'] = updatedUser.paid = false;
			}
		}

		if (await this.userRepository.save(updatedUser)) {
			await this.logRepository.save({
				log: JSON.stringify(logObject),
			});
			return JSON.stringify({ success: 'User was updated successfully' });
		} else {
			console.log('something went wrong while saving User new info!');
			return JSON.stringify({ success: 'User was not updates. Something went wrong' });
		}
	}

	// function used to update de payment state of the user
	public async updatePaid(requestBody, token) {
		token = token.slice(7);
		token = JWT(token);
		const { id, paid } = requestBody;
		if (token.role === 'admin' || token.role === 'accountant' || token.role === 'event manager') {
			const user = await this.userRepository.findOne(id);
			if (paid === 'true') {
				user.paid = true;
			} else {
				user.paid = false;
			}

			if (await this.userRepository.save(user)) {
				return JSON.stringify({ success: 'Paid was updated!' });
			} else {
				return JSON.stringify({ success: 'Paid update did not work' });
			}
		} else {
			console.log('You are not authorized to update this!');
			return JSON.stringify({ success: 'You are not allowed to perform this action!' });
		}
	}
}
