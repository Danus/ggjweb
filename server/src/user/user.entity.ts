import { Column, Entity, Index, PrimaryGeneratedColumn, ManyToMany, JoinTable, OneToMany } from 'typeorm';
import { Group } from '../group/group.entity';

@Entity()
export class User {
	@PrimaryGeneratedColumn()
	id: number;

	@Index({ unique: true })
	@Column('varchar', { length: 255 })
	emailAddress: string;

	@Column('varchar', { length: 55 })
	firstName: string;

	@Column('varchar', { length: 55 })
	lastName: string;

	@Column('varchar', { length: 255 })
	password: string;

	@Column('varchar', { length: 255 })
	salt: string;

	@Column('varchar', { length: 20 })
	diet: string;

	@Column('varchar', { length: 255 })
	allergies: string;

	@Column('varchar', { length: 255 })
	skillSet: string;

	@Column('varchar', { length: 255 })
	hardware: string;

	@Column({ default: false })
	sponsorEmailAgreed: boolean;

	@Column('varchar', { length: 20, default: 'user' })
	role: string;

	@Column({ default: false })
	paid: boolean;

	@OneToMany(
		type => Group,
		group => group.leader,
	)
	groupsLeader: Group[];

	@ManyToMany(
		type => Group,
		group => group.users,
	)
	@JoinTable()
	groups: Group[];
}
