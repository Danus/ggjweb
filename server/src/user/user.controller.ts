import { Body, Controller, Get, Headers, Param, Patch, UseGuards } from '@nestjs/common';
import { RestController } from '../rest/rest.controller';
import { User } from './user.entity';
import { Connection } from 'typeorm';
import { UserService } from './user.service';
import { AuthGuard } from '@nestjs/passport';

@Controller('user')
export class UserController extends RestController<User> {
	constructor(connection: Connection, private readonly userService: UserService) {
		super(connection.getRepository(User));
	}

	@UseGuards(AuthGuard('jwt'))
	@Get('noGroup')
	public async getNotInGroup(@Headers() headers) {
		return await this.userService.getNotInGroup(headers.authorization);
	}

	@UseGuards(AuthGuard('jwt'))
	@Get(':id')
	public async getUser(@Headers() headers, @Param() params) {
		return await this.userService.getUser(params.id, headers.authorization);
	}

	@UseGuards(AuthGuard('jwt'))
	@Get('paid')
	public async getUsersPaid(@Headers() headers, @Param() params) {
		// return await this.userService.getUsersPaid(params.id, headers.authorization);
	}

	@UseGuards(AuthGuard('jwt'))
	@Get('unpaid')
	public async getUsersUnPaid(@Headers() headers, @Param() params) {
		// return await this.userService.getUsersPaid(params.id, headers.authorization);
	}

	@UseGuards(AuthGuard('jwt'))
	@Patch('update')
	public async updateUser(@Headers() headers, @Body() requestBody) {
		await this.userService.updateProfile(requestBody, headers.authorization);
	}

	@UseGuards(AuthGuard('jwt'))
	@Patch('paid')
	public async paid(@Headers() headers, @Body() requestBody) {
		await this.userService.updatePaid(requestBody, headers.authorization);
	}
}
