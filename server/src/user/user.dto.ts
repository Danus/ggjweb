export interface UserUpdateDto {
	id: number;
	password: string;
	diet: string;
	allergies: string;
	skillSet: string;
	hardware: string;
}
