import { Injectable } from '@nestjs/common';
import { RestEntity } from './rest.entity';
import { Connection, ObjectType, Repository } from 'typeorm';

@Injectable()
export abstract class RestService<T extends RestEntity<T>> {
	private repository: Repository<T>;

	protected constructor(private readonly connection: Connection, entity: ObjectType<T>) {
		this.repository = connection.getRepository(entity);
	}

	// public get(id: number, token): Promise<T> {
	//     return this.repository.findOne(id);
	// }

	protected getAll(): Promise<T[]> {
		return this.repository.find();
	}

	protected delete(id: number) {
		return this.repository.delete(id);
	}
}
