import { PrimaryGeneratedColumn } from 'typeorm';

export abstract class RestEntity<T> {
	@PrimaryGeneratedColumn()
	id: number;
}
