import { Controller, Delete, Get, Param, Headers, UseGuards } from '@nestjs/common';
import { Repository } from 'typeorm';
import { AuthGuard } from '@nestjs/passport';

@Controller('rest')
export abstract class RestController<T> {
	protected constructor(private readonly repository: Repository<T> ) {}

	@UseGuards(AuthGuard('jwt'))
	@Get('')
	public getAll() {
		return this.repository.find();
	}

	@UseGuards(AuthGuard('jwt'))
	@Get(':id')
	public get(
		@Headers() headers,
		@Param() params,
	) {
		return this.repository.findOne(params.id, headers.authorization);
	}

	@UseGuards(AuthGuard('jwt'))
	@Delete(':id')
	public delete(@Param() params) {
		return this.repository.delete(params.id);
	}
}
