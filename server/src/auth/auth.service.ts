import { Injectable } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { JwtService } from '@nestjs/jwt';
import { RegisterDto } from './auth.dto';
import {User} from '../user/user.entity';

@Injectable()
export class AuthService {
	constructor(private readonly usersService: UserService, private readonly jwtService: JwtService) {}

	public async register(userDTO: RegisterDto) {
		await this.usersService.registerUser(userDTO);
	}

	async validateUser(emailAddress: string, password: string): Promise<User> {
		return await this.usersService.findByLogin({ emailAddress, password });
	}

	async login(user: any) {
		const payload = { emailAddress: user.emailAddress, userId: user.id, role: user.role, firstName: user.firstName };
		return {
			access_token: this.jwtService.sign(payload),
		};
	}
}
