import { Body, Controller, Get, Post, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { RegisterDto } from './auth.dto';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
	constructor(private authService: AuthService) {}
	@UseGuards(AuthGuard('jwt'))
	@Get('isAuthenticated')
	public async isAuthorized() {
		return 'Welcome!';
	}

	@Post('register')
	public async register(@Body() requestBody: RegisterDto) {
		await this.authService.register(requestBody);
	}

	@UseGuards(AuthGuard('local'))
	@Post('login')
	async login(@Request() req) {
		return this.authService.login(req.user);
	}
}
