export interface LoginDto {
	emailAddress: string;
	password: string;
}
export interface RegisterDto {
	emailAddress: string;
	firstName: string;
	lastName: string;
	password: string;
	diet: string;
	allergies: string;
	skillSet: string;
	hardware: string;
	sponsorEmailAgreed: boolean;
	role: string;
	paid: boolean;
}
