import { Body, Controller, Patch, Post, Headers, UseGuards, Get, Param, Delete } from '@nestjs/common';
import { RestController } from '../rest/rest.controller';
import { Group } from './group.entity';
import { Connection } from 'typeorm';
import { GroupService } from './group.service';
import { GroupDto } from './group.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('group')
export class GroupController extends RestController<Group> {
	constructor(connection: Connection, private readonly groupService: GroupService) {
		super(connection.getRepository(Group));
	}

	@UseGuards(AuthGuard('jwt'))
	@Get()
	public async getAllGroups() {
		return await this.groupService.getAllGroups();
	}

	@UseGuards(AuthGuard('jwt'))
	@Get(':id')
	public async getGroup(@Headers() headers, @Param() params) {
		return await this.groupService.getGroup(params.id, headers.authorization);
	}

	@UseGuards(AuthGuard('jwt'))
	@Get('user/:id')
	public getUserGroups(@Headers() headers, @Param() params) {
		return this.groupService.getUserGroups(params.id, headers.authorization);
	}

	@UseGuards(AuthGuard('jwt'))
	@Get('leader/:id')
	public getUserGroupsLeaderOnly(@Headers() headers, @Param() params) {
		return this.groupService.getUserGroupsLeaderOnly(params.id, headers.authorization);
	}

	@UseGuards(AuthGuard('jwt'))
	@Get('leader/:id')
	public(@Headers() headers, @Param() params) {
		return this.groupService.getUserGroupsLeaderOnly(params.id, headers.authorization);
	}

	@UseGuards(AuthGuard('jwt'))
	@Post()
	public createGroup(@Headers() headers, @Body() requestBody: GroupDto) {
		return this.groupService.create(requestBody, headers.authorization);
	}

	@UseGuards(AuthGuard('jwt'))
	@Post('addUser')
	public async addUser(@Headers() headers, @Body() requestBody) {
		return await this.groupService.addUserToGroup(requestBody, headers.authorization);
	}

	@UseGuards(AuthGuard('jwt'))
	@Patch('update')
	public async updateGroup(@Headers() headers, @Body() requestBody) {
		return await this.groupService.updateGroup(requestBody, headers.authorization);
	}

	@UseGuards(AuthGuard('jwt'))
	@Delete('leave/:id')
	public async leaveGroup(@Headers() headers, @Param() params) {
		return await this.groupService.leaveGroup(params.id, headers.authorization);
	}
}
