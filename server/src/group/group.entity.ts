import { Column, Entity, Index, PrimaryGeneratedColumn, ManyToMany, ManyToOne } from 'typeorm';
import { User } from '../user/user.entity';
import { Event } from '../event/event.entity';

@Entity()
export class Group {
	@PrimaryGeneratedColumn()
	id: number;

	@Index({ unique: true })
	@Column('varchar', { length: 55 })
	name: string;

	@Column('varchar', { length: 255, nullable: true })
	password: string;

	@Column('varchar', { length: 255 })
	salt: string;

	@Column('varchar', { length: 255, nullable: true })
	description: string;

	@Column({ default: 'open' })
	openStatus: string;

	@Column({ default: '' })
	url: string;

	@ManyToOne(
		type => User,
		user => user.groupsLeader,
		{ nullable: false },
	)
	leader: User;

	@ManyToMany(
		type => User,
		user => user.groups,
	)
	users: User[];

	@ManyToOne(
		type => Event,
		event => event.groups,
		{ nullable: false },
	)
	event: Event;
}
