import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { getConnection, Repository } from 'typeorm';
import { Group } from './group.entity';
import { InjectRepository } from '@nestjs/typeorm';
import * as crypto from 'crypto';
import { User } from '../user/user.entity';
import * as JWT from 'jwt-decode';
import { Event } from '../event/event.entity';

@Injectable()
export class GroupService {
	@InjectRepository(Group)
	private readonly groupRepository: Repository<Group>;

	@InjectRepository(User)
	private readonly userRepository: Repository<User>;

	@InjectRepository(Event)
	private readonly eventRepository: Repository<Event>;

	private static sanitizeGroup(group: Group): Group {
		const sanitizedGroup = group;
		delete sanitizedGroup.password;
		return sanitizedGroup;
	}

	// GET all groups including the users and the events that are related to that group
	public async getAllGroups() {
		return await this.groupRepository.find({
			select: ['id', 'name', 'openStatus', 'description', 'url'],
			relations: ['event', 'users', 'leader'],
		});
	}

	// GET group with an ID
	public async getGroup(id: number, token) {
		token = token.slice(7);
		token = JWT(token);

		const group = await this.groupRepository.findOne(id, {
			select: ['id', 'name', 'openStatus', 'description', 'url'],
			relations: ['event', 'users', 'leader'],
		});

		if (
			token.role === 'admin' ||
			token.role === 'event manager' ||
			(token.role === 'user' && token.userId === group.leader.id)
		) {
			return group;
		}
	}

	// GET all the groups of the user
	public async getUserGroups(id, token) {
		token = token.slice(7);
		token = JWT(token);

		if (
			(token.role === 'user' && token.userId === Number(id)) ||
			token.role === 'admin' ||
			token.role === 'event manager'
		) {
			const groupsOfUser = [];

			const groupsMember = await getConnection()
				.getRepository(Group)
				.createQueryBuilder()
				.innerJoinAndSelect(
					'user_groups_group',
					'userGroup',
					'userGroup.userId = ' + id + ' and userGroup.groupId = Group.id',
				)
				.innerJoinAndSelect('user', 'usr', 'usr.id = userGroup.userId')
				.getMany();

			const getGroup = async groupId =>
				await this.groupRepository
					.findOne({
						select: ['id', 'name', 'openStatus', 'description', 'url'],
						where: [{ id: groupId }],
						relations: ['event', 'users', 'leader'],
					})
					.then();

			for (const group of groupsMember) {
				groupsOfUser.push(await getGroup(group.id));
			}

			const result = await this.groupRepository.find({
				select: ['id', 'name', 'openStatus', 'description', 'url'],
				where: [{ leader: { id: id } }],
				relations: ['event', 'users', 'leader'],
			});

			for (let i = 0; i < result.length; i++) {
				groupsOfUser.push(result[i]);
			}

			return JSON.stringify(groupsOfUser);
		} else {
			console.log('You are not allowed to perform this action');
			return JSON.stringify({ success : 'You cannot perform this action!'});
		}
	}

	public async getUserGroupsLeaderOnly(id, token) {
		token = token.slice(7);
		token = JWT(token);

		if (token.userId === 'user' || token.role === 'admin' || token.role === 'event manager') {
			const result = await this.groupRepository.find({
				select: ['id', 'name', 'openStatus', 'description', 'url'],
				where: [{ leader: { id: id } }],
				relations: ['event', 'users', 'leader'],
			});
			return JSON.stringify(result);
		} else {
			/*throw new HttpException({
                status: HttpStatus.FORBIDDEN,
                error: 'Invalid Token!<br> You are not allowed to perform this action!',
            }, 403);*/
			const result = await this.groupRepository.find({
				select: ['id', 'name', 'openStatus', 'description', 'url'],
				where: [{ leader: { id: id } }],
				relations: ['event', 'users', 'leader'],
			});
			return JSON.stringify(result);
		}
	}

	// this function creates a new group
	public async create(requestBody, token) {
		token = token.slice(7);
		token = JWT(token);

		const { name, password, description, leaderId, eventId, url } = requestBody;

		const groups = await this.groupRepository.find();

		for (let i = 0; i < groups.length ; i++) {
			if (groups[i].name === name) {
				return JSON.stringify({ success : 'This name is already taken!'});
			}
		}

		if (token.role === 'admin' || token.role === 'event manager' || token.role === 'user') {
			const group = new Group();
			group.name = name;
			group.description = description;
			group.salt = crypto.randomBytes(32).toString('hex');
			group.url = url;
			group.event = eventId;

			// checking if password is available in the request
			if (password) {
				group.password = crypto.pbkdf2Sync(password, group.salt, 10000, 100, 'sha512').toString('hex');
				group.openStatus = 'openPassword';
			} else {
				group.openStatus = 'open';
			}

			if (token.role === 'admin' || token.role === 'event manager') {
				// if the leader is not set the group cannot be created
				if (leaderId) {
					group.leader = await this.userRepository.findOne({ where: { id: leaderId } });

					// if the role of the leader is 'admin' or 'event manager' the group cannot be created
					if (group.leader.role === 'admin' || group.leader.role === 'event manager') {
						console.log('You cannot add a admin as a group leader');
						return JSON.stringify({success : 'Admins and event managers are not allowed to be a group leader!'});
					}
				} else {
					console.log('leader ID wasn\'t set!');
					return JSON.stringify({success : 'Leader ID was not set!'});
				}
			} else {
				group.leader = await this.userRepository.findOne({ where: { id: token.userId } });
			}

			if (!(await this.groupRepository.save(group))) {
				console.log('An error occurred while creating the group!');
				return JSON.stringify({success : 'An error occurred in the update!'});
			} else {
				return JSON.stringify({success : 'Group created successfully!'});
			}
			//
		} else {
			console.log('You are not allowed to execute those queries');
			throw new HttpException(
				{
					status: HttpStatus.FORBIDDEN,
					error: 'Invalid Token!<br> You are not allowed to perform this action!',
				},
				403,
			);
		}
	}

	public async addUserToGroup(requestBody, token) {
		token = token.slice(7);
		token = JWT(token);

		const { id, password } = requestBody;

		if (token.role === 'user') {
			const user = await this.userRepository.findOne(token.userId);
			const group = await this.groupRepository.findOne(id);

			if (group.openStatus === 'openPassword') {
				if (password) {
					if (group.password === crypto.pbkdf2Sync(password, group.salt, 10000, 100, 'sha512').toString('hex')) {
						await getConnection()
							.createQueryBuilder()
							.relation(Group, 'users')
							.of(group)
							.add(user);
						return JSON.stringify({ success: 'true' });
					} else {
						console.log('passwords don\'t match');
						return JSON.stringify({ success: 'false' });
					}
				} else {
					console.log('password not set');
					return JSON.stringify({ success: 'false' });
				}
			} else if (group.openStatus === 'open') {
				await getConnection()
					.createQueryBuilder()
					.relation(Group, 'users')
					.of(group)
					.add(user);
				return JSON.stringify({ success: 'true' });
			} else {
				console.log('you cannot enter a group that is closed');
				throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);
			}
		} else {
			console.log('you cannot join a group');
			return JSON.stringify({ success: 'You cannot add a user to the group' });
		}
	}

	public async updateGroup(requestBody, token) {
		token = token.slice(7);
		token = JWT(token);

		const updatedGroup = await this.groupRepository.findOne(requestBody.id, { relations: ['leader'] });
		const { name, password, description, url, openStatus, eventId, members } = requestBody;

		const groups = await this.groupRepository.find();

		for (let i = 0; i < groups.length ; i++) {
			if (groups[i].name === name) {
				return JSON.stringify({ success : 'This name is already taken!'});
			}
		}

		if (token.userId === updatedGroup.leader.id || token.role === 'admin' || token.role === 'event manager') {
			// checking for element in the requestBody if the various attributes are set
			if (name) {
				updatedGroup.name = requestBody.name;
			}

			if (password && openStatus) {
				if (openStatus === 'closed') {
					updatedGroup.password = null;
					updatedGroup.openStatus = openStatus;
				} else {
					updatedGroup.password = crypto.pbkdf2Sync(password, updatedGroup.salt, 10000, 100, 'sha512').toString('hex');
					updatedGroup.openStatus = 'openPassword';
				}
			} else if (!password && openStatus) {
					updatedGroup.password = null;
					updatedGroup.openStatus = openStatus;
			} else if (password && !openStatus) {
				if (updatedGroup.openStatus !== 'closed') {
					updatedGroup.password = crypto.pbkdf2Sync(password, updatedGroup.salt, 10000, 100, 'sha512').toString('hex');
					updatedGroup.openStatus = 'openPassword';
				}
			}

			if (description !== null) {
				updatedGroup.description = description;
			}
			if (url) {
				updatedGroup.url = url;
			}
			if (eventId) {
				updatedGroup.event = eventId;
			}
			if (members) {
				if (members.length !== 0) {
					for (let i = 0; i < members.length; i++) {
						// getting the member being removed
						const removingMember = await this.userRepository.findOne(members[i]);
						// removing the previous queried member
						await getConnection()
							.createQueryBuilder()
							.relation(Group, 'users')
							.of(updatedGroup)
							.remove(removingMember);
					}
				}
			}
			if (await this.groupRepository.save(updatedGroup)) {
				return JSON.stringify({ success: 'Group updated successfully!' });
			} else {
				return JSON.stringify({ success: 'Something went wrong!' });
			}
		} else {
			return JSON.stringify({ success: 'You are not allowed to perform this action!' });
		}
	}

	public async leaveGroup(id, token) {
		token = token.slice(7);
		token = JWT(token);

		id = Number(id);

		if (token.role === 'user') {
			const userGroups = await getConnection()
				.getRepository(User)
				.createQueryBuilder()
				.innerJoinAndSelect(
					'user_groups_group',
					'userGroup',
					'userGroup.userId = ' + token.userId + ' and userGroup.groupId = ' + id,
				)
				.innerJoinAndSelect('user', 'usr', 'usr.id = userGroup.userId')
				.getOne();

			if (userGroups) {
				const user = await this.userRepository.findOne(token.userId);
				const group = await this.groupRepository.findOne(id);
				await getConnection()
					.getRepository(User)
					.createQueryBuilder()
					.relation(Group, 'users')
					.of(group)
					.remove(user);
				return JSON.stringify({ success: 'true' });
			} else {
				return JSON.stringify({ success: 'false' });
			}
		} else {
			return JSON.stringify({ success: 'false' });
		}
	}
}
