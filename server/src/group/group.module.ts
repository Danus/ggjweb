import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GroupService } from './group.service';
import { Group } from './group.entity';
import {User} from '../user/user.entity';
import { Event } from '../event/event.entity';

@Module({
	imports: [TypeOrmModule.forFeature([Group, User, Event])],
	providers: [GroupService],
	exports: [GroupService],
})
export class GroupModule {}
