export interface GroupDto {
	name: string;
	password: string;
	description: string;
	url: string;
}

