import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Log } from './log.entity';
import { Repository } from 'typeorm';

@Injectable()
export class LogService {
    @InjectRepository(Log)
    private readonly userRepository: Repository<Log>;
}
