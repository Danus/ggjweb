import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Log {
	@PrimaryGeneratedColumn()
	id: number;

	@Column('varchar', { length: 255 })
	log: string;

	@Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
	timestamp: string;
}
