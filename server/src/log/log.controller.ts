import { Controller } from '@nestjs/common';
import { Connection } from 'typeorm';
import { RestController } from '../rest/rest.controller';
import { Log } from './log.entity';
import { LogService } from './log.service';

@Controller('log')
export class LogController extends RestController<Log> {
    constructor(connection: Connection, private readonly logService: LogService) {
        super(connection.getRepository(Log));
    }
}
