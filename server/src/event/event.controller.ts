import { Body, Controller, Get, Headers, Param, Patch, Post, UseGuards } from '@nestjs/common';
import { RestController } from '../rest/rest.controller';
import { Event } from './event.entity';
import { Connection } from 'typeorm';
import { AuthGuard } from '@nestjs/passport';
import { EventService } from './event.service';

@Controller('event')
export class EventController extends RestController<Event> {
	constructor(connection: Connection, private readonly eventService: EventService) {
		super(connection.getRepository(Event));
	}

	@Get('')
	public async getAllEvent() {
		return await this.eventService.getAllEvents();
	}

	@UseGuards(AuthGuard('jwt'))
	@Get('group/:id')
	public getGroupsByEvent(@Headers() headers, @Param() params) {
		return this.eventService.getGroupsByEvent(params.id, headers.authorization);
	}

	@Get('preview')
	public async getPreview() {
		return await this.eventService.getPreview();
	}

	@Get(':id')
	public async getEvent(@Param() params) {
		return await this.eventService.getEvent(params.id);
	}

	@UseGuards(AuthGuard('jwt'))
	@Post('')
	public async createEvent(@Headers() headers, @Body() requestBody) {
		return await this.eventService.createEvent(requestBody, headers.authorization);
	}

	@UseGuards(AuthGuard('jwt'))
	@Post('addUser')
	public async addUser(@Headers() headers, @Body() requestBody) {
		return await this.eventService.addUserToEvent(requestBody, headers.authorization);
	}

	@UseGuards(AuthGuard('jwt'))
	@Patch('update')
	public async updateEvent(@Headers() headers, @Body() requestBody) {
		return await this.eventService.updateEvent(requestBody, headers.authorization);
	}
}
