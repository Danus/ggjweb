import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EventService } from './event.service';
import { Event } from './event.entity';
import { User } from '../user/user.entity';
import { Group } from '../group/group.entity';

@Module({
	imports: [TypeOrmModule.forFeature([Event, User, Group])],
	providers: [EventService],
	exports: [EventService],
})
export class EventModule {}
