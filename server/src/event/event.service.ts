import { Injectable } from '@nestjs/common';
import { getConnection, Repository } from 'typeorm';
import { Event } from './event.entity';
import { InjectRepository } from '@nestjs/typeorm';
import * as JWT from 'jwt-decode';
import { User } from '../user/user.entity';
import { Group } from '../group/group.entity';

@Injectable()
export class EventService {
	@InjectRepository(Group)
	private readonly groupRepository: Repository<Group>;

	@InjectRepository(Event)
	private readonly eventRepository: Repository<Event>;

	@InjectRepository(User)
	private readonly userRepository: Repository<User>;

	private static convertToDate(date, time) {
		date = new Date(date);
		time = time.split(':');
		date.setUTCHours(time[0], time[1]);
		return date;
	}

	// get all events
	public async getPreview() {
		return getConnection()
			.getRepository(Event)
			.createQueryBuilder()
			.orderBy('startDate', 'ASC')
			.limit(2)
			.getMany();
	}

	// get all events
	public async getAllEvents() {
		return await this.eventRepository.find({ order: { startDate: 'ASC' } });
	}

	// get all events with relations
	public async getEventWithRelations() {
		return await this.eventRepository.find({ order: { id: 'DESC' }, relations: ['users', 'groups'] });
	}

	// get a single event
	public async getEvent(eventID) {
		return await this.eventRepository.findOne(eventID);
	}

	// get all groups from event ID
	public async getGroupsByEvent(id, token: any) {
		token = token.slice(7);
		token = JWT(token);

		if (token.role === 'admin' || token.role === 'event manager') {
			return await this.groupRepository.find({
				select: ['id', 'name', 'openStatus', 'description', 'url'],
				where: [{ event: { id: id } }],
				relations: ['event', 'users', 'leader'],
			});
		} else {
			console.log('you are not allowed to perform this action!');
			return JSON.stringify({ success: 'false' });
		}
	}

	// create an event
	public async createEvent(requestBody, token) {
		token = token.slice(7);
		token = JWT(token);

		const { name, description, prerequisites, address, startTime, endTime, imageURL } = requestBody;
		let { startDate, endDate } = requestBody;

		if (token.role === 'admin' || token.role === 'event manager') {
			const event = new Event();
			event.name = name;
			event.description = description;
			event.prerequisites = prerequisites;
			event.address = address;
			startDate = startDate + 'Z';
			event.startDate = EventService.convertToDate(startDate, startTime);
			endDate = endDate + 'Z';
			event.endDate = EventService.convertToDate(endDate, endTime);
			event.imageURL = imageURL;

			if (await this.eventRepository.save(event)) {
				return JSON.stringify({ success: 'true' });
			} else {
				console.log('something went wrong while adding event');
				return JSON.stringify({ success: 'false' });
			}
		} else {
			console.log('you are not allowed to perform this action!');
			return JSON.stringify({ success: 'false' });
		}
	}

	// update an event
	public async updateEvent(requestBody, token) {
		token = token.slice(7);
		token = JWT(token);

		const {
			id,
			name,
			description,
			prerequisites,
			address,
			startDate,
			startTime,
			endDate,
			endTime,
			imageURL,
		} = requestBody;

		if (token.role === 'admin' || token.role === 'event manager') {
			const event = await this.eventRepository.findOne(id);

			if (name) {
				event.name = name;
			}
			if (description) {
				event.description = description;
			}
			if (prerequisites) {
				event.prerequisites = prerequisites;
			}
			if (address) {
				event.address = address;
			}

			if (startDate && startTime) {
				const startDateUTC = startDate + 'Z';
				event.startDate = EventService.convertToDate(startDateUTC, startTime);
			} else if (startDate && !startTime) {
				const time = await this.eventRepository.findOne(id, { select: ['startDate'] });
				// @ts-ignore
				const stringTime = new Date(time.startDate).toLocaleTimeString('us-US', { timeZone: 'UTC' });
				const startDateUTC = startDate + 'Z';
				event.startDate = EventService.convertToDate(startDateUTC, stringTime);
			} else if (!startDate && startTime) {
				const date = await this.eventRepository.findOne(id, { select: ['startDate'] });
				// @ts-ignore
				const stringDate = new Date(date.startDate).toLocaleDateString('us-US', { timeZone: 'UTC' });
				const startDateUTC = stringDate + 'Z';
				event.startDate = EventService.convertToDate(startDateUTC, startTime);
			}

			if (endDate && endTime) {
				const endDateUTC = endDate + 'Z';
				event.endDate = EventService.convertToDate(endDateUTC, endTime);
			} else if (endDate && !endTime) {
				const time = await this.eventRepository.findOne(id, { select: ['endDate'] });
				// @ts-ignore
				const stringTime = new Date(time.endDate).toLocaleTimeString('us-US', { timeZone: 'UTC' });
				const endDateUTC = endDate + 'Z';
				event.endDate = EventService.convertToDate(endDateUTC, stringTime);
			} else if (!endDate && endTime) {
				const date = await this.eventRepository.findOne(id, { select: ['endDate'] });
				console.log(endTime);
				// @ts-ignore
				const stringDate = new Date(date.endDate).toLocaleDateString('us-US', { timeZone: 'UTC' });
				const endDateUTC = stringDate + 'Z';
				event.endDate = EventService.convertToDate(endDateUTC, endTime);
			}

			if (imageURL) {
				event.imageURL = imageURL;
			}

			if (await this.eventRepository.save(event)) {
				console.log('event updated');
				return JSON.stringify({ success: 'true' });
			} else {
				return JSON.stringify({ success: 'false' });
			}
		} else {
			console.log('you are not allowed to update an event');
			return JSON.stringify({ success: 'false' });
		}
	}

	// add a user to the event
	public async addUserToEvent(requestBody, token) {
		token = token.slice(7);
		token = JWT(token);

		const { id } = requestBody;

		if (token.role === 'user') {
			const user = await this.userRepository.findOne(token.userId);
			const event = await this.eventRepository.findOne(id);

			await getConnection()
				.createQueryBuilder()
				.relation(Event, 'users')
				.of(event)
				.add(user);
			user.paid = false;
			await this.userRepository.save(user);

			return JSON.stringify({ success: 'false' });
		} else {
			console.log('you cannot join a group');
			return JSON.stringify({ success: 'false' });
		}
	}
}
