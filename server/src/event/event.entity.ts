import { Column, Entity, Index, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Group } from '../group/group.entity';
import DateTimeFormat = Intl.DateTimeFormat;
import { text } from 'express';

@Entity()
export class Event {
	@PrimaryGeneratedColumn()
	id: number;

	@Index({ unique: true })
	@Column('varchar', { length: 55 })
	name: string;

	@Column('datetime')
	startDate: DateTimeFormat;

	@Column('datetime')
	endDate: DateTimeFormat;

	@Column('varchar', { length: 1000 })
	description: string;

	@Column('varchar', { length: 1000 })
	prerequisites: string;

	@Column('varchar', { length: 255 })
	address: string;

	@Column('varchar', { length: 600 })
	imageURL: string;

	@OneToMany(
		type => Group,
		group => group.event,
	)
	groups: Group[];
}
