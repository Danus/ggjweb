import { Module } from '@nestjs/common';
import { AngularUniversalModule, applyDomino } from '@nestjs/ng-universal';
import { join } from 'path';
import { AppController } from './app.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './src/auth/auth.module';
import {User} from './src/user/user.entity';
import { RestModule } from './src/rest/rest.module';
import { UserController } from './src/user/user.controller';
import { Group } from './src/group/group.entity';
import { GroupController } from './src/group/group.controller';
import { Event } from './src/event/event.entity';
import { EventController } from './src/event/event.controller';
import { dbInfo } from './dbInformation';
import { GroupModule } from './src/group/group.module';
import { UserModule } from './src/user/user.module';
import { LogController } from './src/log/log.controller';
import { LogModule } from './src/log/log.module';
import { Log } from './src/log/log.entity';
import { EventModule } from './src/event/event.module';

const BROWSER_DIR = join(process.cwd(), 'dist/browser');
applyDomino(global, join(BROWSER_DIR, 'index.html'));

@Module({
	imports: [
		AngularUniversalModule.forRoot({
			viewsPath: BROWSER_DIR,
			bundle: require('../server/main'),
			liveReload: true,
		}),
		TypeOrmModule.forRoot({
			type: 'mysql',
			host: dbInfo.host,
			port: dbInfo.port,
			username: dbInfo.username,
			password: dbInfo.password,
			database: dbInfo.database,
			entities: [User, Group, Event, Log],
			synchronize: true,
		}),
		GroupModule,
		AuthModule,
		RestModule,
		UserModule,
		LogModule,
		EventModule,
	],
	controllers: [AppController, UserController, GroupController, EventController, LogController],
	providers: [],
})
export class ApplicationModule {}
