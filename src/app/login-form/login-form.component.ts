import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { JwtHelper } from '../jwtHelper';

@Component({
	selector: 'app-login-form',
	templateUrl: './login-form.component.html',
	styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit {
	constructor(private loginService: LoginService, private cookieService: CookieService, private router: Router) {}

	public errorMessage: string;

	public loginCred = new FormGroup({
		email: new FormControl('', [Validators.required, Validators.email]),
		password: new FormControl('', Validators.required),
	});

	ngOnInit() {
		const token: any = JwtHelper.decodeToken(this.cookieService.get('loginTokenGGJ'));
		if (token.userId !== undefined) {
			this.router.navigateByUrl('/').then();
		}
	}

	private loginSuccess(token: any): void {
		const date = new Date();
		// Set it expire in 5 days
		date.setTime(date.getTime() + 5 * 24 * 60 * 60 * 1000);
		this.cookieService.set('loginTokenGGJ', token.access_token, date);
		this.errorMessage = '';
		this.router.navigateByUrl('/').then();
	}

	private loginError(error: any): void {
		this.errorMessage = error.error.message;
	}

	public login(): void {
		if (this.loginCred.valid) {
			this.loginService.login(this.loginCred.get('email').value, this.loginCred.get('password').value).subscribe(
				data => this.loginSuccess(data),
				error => this.loginError(error),
			);
		} else {
			this.errorMessage = 'Enter your email or/and password.';
		}
	}
}
