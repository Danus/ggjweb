import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { GroupService } from '../group.service';
import { CookieService } from 'ngx-cookie-service';
import { JwtHelper } from '../jwtHelper';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
	selector: 'app-group-management',
	templateUrl: './group-management.component.html',
	styleUrls: ['./group-management.component.scss'],
})
export class GroupManagementComponent implements OnInit {
	// private dataToSend: { [k: string]: any } = {};
	public leader: string;
	public errorMessage: string;
	public successMessage: string;
	public token: any;
	// // Subscription (to hand async data)
	private subscription: Subscription;
	public events = [];
	public usersInGroup = [];
	public usersToRemove = [];
	public groupEvent: any;
	private groupId: number;
	// data that will be sent to be updated
	public data: { [k: string]: any } = {};

	public done = false;

	constructor(
		private groupService: GroupService,
		private cookieService: CookieService,
		private route: ActivatedRoute,
		private router: Router,
	) {}

	public updateGroup: FormGroup = new FormGroup({
		name: new FormControl('', [Validators.required, Validators.maxLength(55)]),
		password: new FormControl('', Validators.minLength(8)),
		description: new FormControl('', Validators.maxLength(255)),
		url: new FormControl('', Validators.maxLength(255)),
		event: new FormControl('', [Validators.required]),
		status: new FormControl('', [Validators.required]),
	});

	// Control if changes were made
	public changedValue = {
		name: false,
		password: false,
		description: false,
		status: false,
		event: false,
		url: false,
	};
	public optionsStatus = ['Open', 'Closed'];
	public status: boolean;
	public groupStatus: string;

	private redirectLogin() {
		this.router.navigateByUrl('/login').then();
	}

	private redirectGroup() {
		this.router.navigateByUrl('/group').then();
	}

	// Control if changes were made in the form
	public changesMade(): boolean {
		return !(
			!this.changedValue.name &&
			!this.changedValue.event &&
			!this.changedValue.url &&
			!this.changedValue.description &&
			!this.changedValue.status &&
			this.usersToRemove.length === 0 &&
			this.updateGroup.get('password').value === ''
		);
	}

	ngOnInit() {
		const token = JwtHelper.decodeToken(this.cookieService.get('loginTokenGGJ'));
		this.token = token;
		if (token.userId === undefined) {
			this.redirectLogin();
		} else {
			this.subscription = this.groupService.getEvents().subscribe(
				data => this.setEvents(data),
				error => console.log(error),
			);

			this.subscription = this.groupService.getGroup(this.getId()).subscribe(
				data => this.setData(data),
				error => this.redirectGroup(),
			);
		}
	}

	private getId(): number {
		return +this.route.snapshot.paramMap.get('id');
	}

	private setData(data): void {
		if (this.token.role !== 'admin' && this.token.role !== 'event manager' && data === null) {
			this.router.navigateByUrl('/').then();
		}
		this.groupId = data.id;
		this.updateGroup.controls['name'].setValue(data.name);
		this.updateGroup.controls['description'].setValue(data.description);
		if (data.openStatus !== 'closed') {
			this.updateGroup.controls['status'].setValue('Open');
			this.groupStatus = 'Open';
		} else {
			this.updateGroup.controls['status'].setValue('Closed');
			this.groupStatus = 'Closed';
		}
		this.leader = data.leader.firstName + ' ' + data.leader.lastName;
		if (data.users.length > 0) {
			for (let i = 0; i < data.users.length; i++) {
				const userToAdd = { id: data.users[i].id, name: data.users[i].firstName + ' ' + data.users[i].lastName };
				this.usersInGroup.push(userToAdd);
			}
		}
		this.updateGroup.controls['event'].setValue(data.event.id);
		this.groupEvent = data.event.id;
		this.updateGroup.controls['url'].setValue(data.url);
		this.done = true;
	}

	public setEvents(data): void {
		for (const event of data) {
			const eventToAdd = { id: event.id, name: event.name };
			this.events.push(eventToAdd);
		}
	}

	public update() {
		if (this.updateGroup.valid) {

			this.errorMessage = '';
			this.successMessage = '';

			this.data.id = this.groupId;

			// Controls if changes were made, if no nothing gets submitted
			if (!this.changesMade()) {
				this.errorMessage = 'No changes made!';
				return;
			}

			if (this.updateGroup.get('password').value !== '') {
				this.data.password = this.updateGroup.get('password').value;
				this.updateGroup.controls['password'].setValue('');
			}
			if (this.changedValue.description) {
				this.data.description = this.updateGroup.get('description').value;
				this.changedValue.description = false;
			}
			if (this.changedValue.name) {
				this.data.name = this.updateGroup.get('name').value;
				this.changedValue.name = false;
			}
			if (this.changedValue.event) {
				this.data.eventId = this.updateGroup.get('event').value;
				this.changedValue.event = false;
			}
			if (this.changedValue.status) {
				this.data.openStatus = this.updateGroup.get('status').value.toLowerCase();
				this.changedValue.status = false;
			}
			if (this.changedValue.url) {
				this.data.url = this.updateGroup.get('url').value.toLowerCase();
				this.changedValue.url = false;
			}
			if (this.usersToRemove.length > 0) {
				this.data.members = this.usersToRemove;
				this.usersToRemove = [];
			}

			// sends updated data
			this.groupService.update(this.data).subscribe(
				data => this.setMessage(data),
				error => (this.errorMessage = 'Error occurred updating your group.'),
			);
		}
	}

	private setMessage(message: any) {
		if (message.success === 'This name is already taken!') {
			this.errorMessage = message.success;
		} else if (message.success === 'Group updated successfully!') {
			this.successMessage = message.success;
		} else {
			this.errorMessage = message.success;
		}
	}

	public delete(id: number) {
		this.usersToRemove.push(id);
		for (let i = 0; i < this.usersInGroup.length; i++) {
			if (this.usersInGroup[i].id === id) {
				this.usersInGroup.splice(i, 1);
				break;
			}
		}
	}
}
