import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { UsersService } from '../users.service';
import { CookieService } from 'ngx-cookie-service';
import { CustomValidators } from '../profile-management/custom-validators';
import { JwtHelper } from '../jwtHelper';
import { map, startWith } from 'rxjs/internal/operators';
import { Router } from '@angular/router';

export interface User {
	id: number;
	emailAddress: string;
	firstName: string;
	lastName: string;
	password: string;
	salt: string;
	diet: string;
	allergies: string;
	skillSet: string;
	hardware: string;
	sponsorEmailAgreed: boolean;
	role: string;
	paid: boolean;
}

@Component({
	selector: 'app-update-profile',
	templateUrl: './profile-management-admin.component.html',
	styleUrls: ['./profile-management-admin.component.scss'],
})

// tslint:disable-next-line:component-class-suffix
export class ProfileManagementAdminComponent implements OnInit {
	public userCtrl = new FormControl('', Validators.required);
	public filteredUsers: Observable<User[]>;
	public users: User[];
	public role: string;

	// formgroup to control the input fields
	public updateProfile: FormGroup;
	// Subscription (to hand async data)
	private subscription: Subscription;
	// error message that will be displayed if an error happens
	public errorMessage: string;
	public successMessage: string;

	// data gotten from the server that can be updated
	public userData: User;
	// data that will be sent to be updated
	public data: { [k: string]: any } = {};

	// Option a user has to 'eat'
	public optionsEat = ['None', 'Vegan', 'Vegetarian'];

	// Options for role selection
	public optionsRole = ['User', 'Accountant', 'Event Manager', 'Admin'];

	// Control if changes were made
	public changedValue = {
		hardware: false,
		allergies: false,
		skillSet: false,
		diet: false,
		role: false,
		paid: false,
	};

	constructor(
		private userService: UsersService,
		private cookieService: CookieService,
		private fb: FormBuilder,
		private router: Router,
	) {
		this.updateProfile = this.fb.group(
			{
				diet: ['', Validators.compose([Validators.required])],
				skillSet: [''],
				allergies: [''],
				paid: [''],
				hardware: ['', Validators.compose([Validators.required])],
				role: ['', Validators.compose([Validators.required])],
				password: [
					'',
					Validators.compose([
						// check whether the entered password has a 8 characters
						Validators.minLength(8),
					]),
				],
				confirmPassword: [''],
			},
			{
				// check whether our password and confirm password match
				validator: CustomValidators.passwordMatchValidator,
			},
		);
	}

	ngOnInit() {
		const token: any = JwtHelper.decodeToken(this.cookieService.get('loginTokenGGJ'));
		if (token.userId === undefined) {
			this.router.navigateByUrl('/login').then();
		} else {
			if (token.role !== 'admin' && token.role !== 'event manager') {
				this.router.navigateByUrl('/').then();
			}
			this.role = token.role;
			this.subscription = this.userService.getAll().subscribe(
				data => this.setDataIntoSearch(data),
				error => console.log(error),
			);
		}
	}

	private setDataIntoSearch(data): void {
		// Taken from: https://stackblitz.com/angular/njnvoyxdgrl?file=src%2Fapp%2Fautocomplete-overview-example.ts
		this.users = <User[]>data;
		this.filteredUsers = this.userCtrl.valueChanges.pipe(
			startWith(''),
			map(user => (user ? this._filterUsers(user) : this.users.slice())),
		);
	}

	private _filterUsers(value: string): User[] {
		const filterValue = value;
		return this.users.filter(user => user.firstName.indexOf(filterValue) === 0);
	}

	// set the data in form inputs
	private setData(obj: User): void {
		this.userData = obj;
		this.updateProfile.controls['hardware'].setValue(this.userData.hardware);
		this.updateProfile.controls['skillSet'].setValue(this.userData.skillSet);
		this.updateProfile.controls['allergies'].setValue(this.userData.allergies);
		this.updateProfile.controls['diet'].setValue(this.userData.diet);
		this.updateProfile.controls['role'].setValue(this.userData.role);
		this.updateProfile.controls['paid'].setValue(this.userData.paid);
	}

	// Control if changes were made in the form
	public changesMade(): boolean {
		return !(
			!this.changedValue.role &&
			!this.changedValue.skillSet &&
			!this.changedValue.hardware &&
			!this.changedValue.allergies &&
			!this.changedValue.diet &&
			!this.changedValue.paid &&
			this.updateProfile.get('password').value === ''
		);
	}

	// update user profile data
	public update(): void {
		if (this.updateProfile.valid) {
			this.data.id = this.userData.id;

			this.successMessage = '';
			this.errorMessage = '';

			// Controls if changes were made, if no nothing gets submitted
			if (!this.changesMade()) {
				this.errorMessage = 'No changes made!';
				return;
			}

			if (this.updateProfile.get('password').value !== '') {
				this.data.password = this.updateProfile.get('password').value;
				this.updateProfile.controls['password'].setValue('');
				this.updateProfile.controls['confirmPassword'].setValue('');
			}
			if (this.changedValue.hardware) {
				this.data.hardware = this.updateProfile.get('hardware').value;
				this.changedValue.hardware = false;
			}
			if (this.changedValue.allergies) {
				this.data.allergies = this.updateProfile.get('allergies').value;
				this.changedValue.allergies = false;
			}
			if (this.changedValue.skillSet) {
				this.data.skillSet = this.updateProfile.get('skillSet').value;
				this.changedValue.skillSet = false;
			}
			if (this.changedValue.diet) {
				this.data.diet = this.updateProfile.get('diet').value;
				this.changedValue.diet = false;
			}
			if (this.changedValue.role) {
				this.data.role = this.updateProfile.get('role').value.toLowerCase();
				this.changedValue.role = false;
			}
			if (this.changedValue.paid) {
				if (this.updateProfile.get('paid').value === true) {
					this.data.paid = 'true';
				} else {
					this.data.paid = 'false';
				}
				this.changedValue.paid = false;
			}

			// sends updated data
			this.userService.update(this.data).subscribe(
				data => (this.successMessage = 'Profile updated.'),
				error => (this.errorMessage = 'Error occurred updating your profile.'),
			);
		}
	}
}
