import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileManagementAdminComponent } from './profile-management-admin.component';

describe('UpdateProfileComponent', () => {
  let component: ProfileManagementAdminComponent;
  let fixture: ComponentFixture<ProfileManagementAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileManagementAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileManagementAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
