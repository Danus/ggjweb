import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { EventService } from '../event.service';
import { JwtHelper } from '../jwtHelper';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-create-event',
	templateUrl: './create-event.component.html',
	styleUrls: ['./create-event.component.scss'],
})
export class CreateEventComponent implements OnInit {
	public errorMessage: string;
	// data that will be sent to be updated
	public data: { [k: string]: any } = {};
	private token: any;

	public successMessage: string;

	public createEvent: FormGroup = new FormGroup({
		name: new FormControl('', [Validators.required, Validators.maxLength(55)]),
		description: new FormControl('', [Validators.required, Validators.maxLength(255)]),
		prerequisites: new FormControl('', [Validators.required, Validators.maxLength(255)]),
		address: new FormControl('', [Validators.required]),
		startDate: new FormControl('', [Validators.required]),
		endDate: new FormControl('', [Validators.required]),
		startTime: new FormControl('', [Validators.required]),
		endTime: new FormControl('', [Validators.required]),
		url: new FormControl('', [Validators.required, Validators.maxLength(255)]),
	});

	constructor(private eventService: EventService, private cookieService: CookieService, private router: Router) {}

	ngOnInit() {
		const token: any = JwtHelper.decodeToken(this.cookieService.get('loginTokenGGJ'));
		this.token = token;
		if (token.userId === undefined) {
			this.router.navigateByUrl('/login').then();
		} else {
			if (token.role !== 'admin' && token.role !== 'event manager') {
				this.router.navigateByUrl('/').then();
			}
		}
	}

	public setSuccessMessage(data: any) {
		if (data.success === 'true') {
			this.successMessage = 'Event Created!';
		} else {
			this.errorMessage = 'Error creating the event!';
		}
	}

	public create() {
		if (this.createEvent.valid) {
			// Gets both end and start time of the event
			const startTime = this.createEvent.get('startTime').value;
			const endTime = this.createEvent.get('endTime').value;

			this.successMessage = '';
			this.errorMessage = '';

			// converts the time into hours and minutes
			const startTimeHours = parseInt(startTime.slice(0, 2), 10);
			const startTimeMinutes = parseInt(startTime.slice(3, 5), 10);
			const endTimeHours = parseInt(endTime.slice(0, 2), 10);
			const endTimeMinutes = parseInt(endTime.slice(3, 5), 10);

			// creates the date of the start and end of the event with the time
			const endDate = new Date(this.createEvent.get('endDate').value).setHours(endTimeHours, endTimeMinutes, 0, 0);
			const startDate = new Date(this.createEvent.get('startDate').value).setHours(
				startTimeHours,
				startTimeMinutes,
				0,
				0,
			);

			if (endDate <= startDate) {
				this.errorMessage = 'Check your dates!';
				return;
			}

			const startDateInForm = this.createEvent.get('startDate').value;
			const formatted_startDate =
				startDateInForm.getFullYear() + '-' + (startDateInForm.getMonth() + 1) + '-' + startDateInForm.getDate();

			const endDateInForm = this.createEvent.get('endDate').value;
			const formatted_endDate =
				endDateInForm.getFullYear() + '-' + (endDateInForm.getMonth() + 1) + '-' + endDateInForm.getDate();
			this.data.name = this.createEvent.get('name').value;
			this.data.description = this.createEvent.get('description').value;
			this.data.prerequisites = this.createEvent.get('prerequisites').value;
			this.data.address = this.createEvent.get('address').value;
			this.data.startTime = this.createEvent.get('startTime').value;
			this.data.endTime = this.createEvent.get('endTime').value;
			this.data.startDate = formatted_startDate;
			this.data.endDate = formatted_endDate;
			this.data.imageURL = this.createEvent.get('url').value;

			console.log(this.data);
			// sends updated data
			this.eventService.createEvent(this.data).subscribe(
				data => this.setSuccessMessage(data),
				error => (this.errorMessage = 'Error creating the event!'),
			);
		}
	}
}
