import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  _url: string;
  constructor(private _http: HttpClient, private cookieService: CookieService) { }

  private httpOptions = {
    headers: new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    })
  };

  public addUserGroup(data: any) {
    this._url = '/group/addUser';
    return this._http.post(this._url, data, this.httpOptions);
  }

  public createGroup(data: any) {
    this._url = '/group';
    return this._http.post(this._url, data, this.httpOptions);
  }

  public getGroups() {
    this._url = '/group';
    return this._http.get(this._url, this.httpOptions);
  }

  public getGroup(id: number) {
    this._url = '/group/' + id ;
    return this._http.get(this._url, this.httpOptions);
  }

  public getGroupsOfUser(id) {
    this._url = '/group/user/' + id;
    return this._http.get(this._url, this.httpOptions);
  }

  public update(data: any) {
    this._url = '/group/update';
    return this._http.patch(this._url, data, this.httpOptions);
  }

  public getEvents() {
    this._url = '/event';
    return this._http.get(this._url, this.httpOptions);
  }

  public leaveGroup(id) {
    this._url = '/group/leave/' + id;
    return this._http.delete(this._url, this.httpOptions);
  }

}
