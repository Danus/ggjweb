import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/internal/operators';
import { CookieService } from 'ngx-cookie-service';
import { JwtHelper } from '../jwtHelper';
import { Router } from '@angular/router';

@Component({
	selector: 'app-main',
	templateUrl: './main.component.html',
	styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit {
	isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
		map(result => result.matches),
		shareReplay(),
	);

	constructor(
		private breakpointObserver: BreakpointObserver,
		private cookieService: CookieService,
		private router: Router,
	) {}

	public navItems: object;
	public dropDowns: object;

	private initNav(token: any) {

		if (token.role === 'user') {
			this.navItems = [
				{
					name: 'Home',
					path: '/',
					icon: 'home',
				},
				{
					name: 'Events',
					path: '/allEvents',
					icon: 'event',
				},
			];
			this.dropDowns = [
				{
					name: token.firstName,
					icon: 'perm_identity',
					items: [
						{
							name: 'Groups',
							path: '/groups',
							icon: 'group',
						},
						{
							name: 'Profile Settings',
							path: '/updateProfile',
							icon: 'settings',
						},
						{
							name: 'Logout',
							path: '/logout',
							icon: 'power_settings_new',
						},
					],
				},
			];
		} else if (token.role === 'admin') {
			this.navItems = [
				{
					name: 'Home',
					path: '/',
					icon: 'home',
				},
				{
					name: 'Events',
					path: '/allEvents',
					icon: 'event',
				},
			];
			this.dropDowns = [
				{
					name: token.firstName,
					icon: 'perm_identity',
					items: [
						{
							name: 'User Management',
							path: '/updateProfileAdmin',
							icon: 'accessibility',
						},
						{
							name: 'Event Management',
							path: '/createEvent',
							icon: 'event_note',
						},
						{
							name: 'Groups',
							path: '/allGroups',
							icon: 'group',
						},
						{
							name: 'Profile Settings',
							path: '/updateProfile',
							icon: 'settings',
						},
						{
							name: 'Logout',
							path: '/logout',
							icon: 'power_settings_new',
						},
					],
				},
			];
		} else if (token.role === 'event manager') {
			this.navItems = [
				{
					name: 'Home',
					path: '/',
					icon: 'home',
				},
				{
					name: 'Events',
					path: '/allEvents',
					icon: 'event',
				},
			];
			this.dropDowns = [
				{
					name: token.firstName,
					icon: 'perm_identity',
					items: [
						{
							name: 'User Management',
							path: '/updateProfileAdmin',
							icon: 'accessibility',
						},
						{
							name: 'Event Management',
							path: '/createEvent',
							icon: 'event_note',
						},
						{
							name: 'Groups',
							path: '/allGroups',
							icon: 'group',
						},
						{
							name: 'Profile Settings',
							path: '/updateProfile',
							icon: 'settings',
						},
						{
							name: 'Logout',
							path: '/logout',
							icon: 'power_settings_new',
						},
					],
				},
			];
		} else if (token.role === 'accountant') {
			this.navItems = [
				{
					name: 'Home',
					path: '/',
					icon: 'home',
				},
				{
					name: 'Events',
					path: '/allEvents',
					icon: 'event',
				},
			];
			this.dropDowns = [
				{
					name: token.firstName,
					icon: 'perm_identity',
					items: [
						{
							name: 'Payment Management',
							path: '/managePayment',
							icon: 'payment',
						},
						{
							name: 'Profile Settings',
							path: '/updateProfile',
							icon: 'settings',
						},
						{
							name: 'Logout',
							path: '/logout',
							icon: 'power_settings_new',
						},
					],
				},
			];
		} else {
			this.navItems = [
				{
					name: 'Home',
					path: '/',
					icon: 'home',
				},
				{
					name: 'Events',
					path: '/allEvents',
					icon: 'event',
				},
				{
					name: 'Login',
					path: '/login',
					icon: 'person',
				},
				{
					name: 'Register',
					path: '/register',
					icon: 'person_add',
				},
			];
			this.dropDowns = [];
		}
	}

	ngOnInit() {
		const token = JwtHelper.decodeToken(this.cookieService.get('loginTokenGGJ'));
		this.initNav(token);
	}
}
