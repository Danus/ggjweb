import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EventService } from '../event.service';
import * as XLSX from 'xlsx';
import {CookieService} from 'ngx-cookie-service';
import {JwtHelper} from '../jwtHelper';

export interface Event {
	name: string;
	startDate: string;
	endDate: string;
}

export interface Group {
	id: number;
	name: string;
	leader: string;
}

export interface User {
	name: string;
	hardware: string;
	allergies: string;
	skillSet: string;
	diet: string;
	paid: boolean;
	groupId: number;
}

@Component({
	selector: 'app-event-detail',
	templateUrl: './event-detail.component.html',
	styleUrls: ['./event-detail.component.scss'],
})
export class EventDetailComponent implements OnInit {
	public event: any;
	public noEvent: boolean;

	public eventExcel: Event;
	public groupsExcel: Group[];
	public usersExcel: User[];

	public token: any;
	public isEmpty: boolean;

	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private eventService: EventService,
		private cookieService: CookieService
	) {}

	ngOnInit() {
		this.eventService.getEvent(this.getIdInURL()).subscribe(
			data => this.setData(data),
			error => console.log(error),
		);

		this.token = JwtHelper.decodeToken(this.cookieService.get('loginTokenGGJ'));
		if (this.token.userId !== undefined) {
			if (this.token.role === 'admin' || this.token.role === 'event manager') {
				this.eventService.getGroupByEvent(Number(this.getIdInURL())).subscribe(
					data => this.generateExcelTable(data),
					error => console.log(error),
				);
			}
		}
	}

	private setData(data: any): void {
		if (data === null) {
			this.noEvent = true;
		} else {
			this.event = data;
			let date = new Date(data.startDate);
			this.event.startDate = date.toLocaleString('en-GB').substr(0, date.toLocaleString('en-GB').length - 3);
			date = new Date(data.endDate);
			this.event.endDate = date.toLocaleString('en-GB').substr(0, date.toLocaleString('en-GB').length - 3);
		}

	}

	public getFilteredUsers(groupId: number) {
		return this.usersExcel.filter(e => e.groupId === groupId);
	}

	private generateExcelTable(data: any) {
		this.groupsExcel = [];
		this.usersExcel = [];

		this.isEmpty = !!data.length;

		if (data.success !== 'false' && data.length > 0) {
			this.eventExcel = {
				name: data[0].event.name,
				startDate: new Date(data[0].event.startDate).toLocaleDateString('en-GB', {timeZone: 'UTC'}),
				endDate: new Date(data[0].event.endDate).toLocaleDateString('en-GB', {timeZone: 'UTC'}),
			};

			for (const group of data) {
				console.log(group);
				if (this.groupsExcel) {
					this.groupsExcel.push({
						id: Number(group.id),
						name: group.name,
						leader: group.leader.firstName + ' ' + group.leader.lastName,
					});

					if (this.usersExcel) {
						this.usersExcel.push({
							name: group.leader.firstName + ' ' + group.leader.lastName,
							hardware: group.leader.hardware,
							allergies: group.leader.allergies,
							skillSet: group.leader.skillSet,
							diet: group.leader.diet,
							paid: Boolean(group.leader.paid),
							groupId: Number(group.id),
						});

						for (const user of group.users) {
							this.usersExcel.push({
								name: user.firstName + ' ' + user.lastName,
								hardware: user.hardware,
								allergies: user.allergies,
								skillSet: user.skillSet,
								diet: user.diet,
								paid: Boolean(user.paid),
								groupId: Number(group.id),
							});
						}
					}
				}
			}
		}
	}

	public exportExcel(): void {
		const fileName = 'GGJEvent.xlsx';

		const eventWs: XLSX.WorkSheet = XLSX.utils.table_to_sheet(document.getElementById('event-table'));

		const wb: XLSX.WorkBook = XLSX.utils.book_new();
		XLSX.utils.book_append_sheet(wb, eventWs, 'GGJEvent');

		for (const group of this.groupsExcel) {
			const groupWs: XLSX.WorkSheet = XLSX.utils.table_to_sheet(document.getElementById('group-table' + group.id));
			XLSX.utils.book_append_sheet(wb, groupWs, 'Group ' + group.name);
		}

		XLSX.writeFile(wb, fileName);
	}

	private getIdInURL(): number {
		return +this.route.snapshot.paramMap.get('id');
	}
}
