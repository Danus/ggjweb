import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { JwtHelper } from '../jwtHelper';
import { map, startWith } from 'rxjs/internal/operators';
import { EventService } from '../event.service';

export interface Event {
	id: number;
	name: string;
	description: string;
	prerequisites: string;
	address: string;
	startDate: any;
	endDate: any;
	imageURL: string;
}
@Component({
	selector: 'app-event-management',
	templateUrl: './event-management.component.html',
	styleUrls: ['./event-management.component.scss'],
})
export class EventManagementComponent implements OnInit {
	public eventCtrl = new FormControl('', Validators.required);
	public filteredEvents: Observable<Event[]>;
	public events: Event[];
	public role: string;

	// formgroup to control the input fields
	public updateEvent: FormGroup;
	// Subscription (to hand async data)
	private subscription: Subscription;
	// error message that will be displayed if an error happens
	public errorMessage: string;
	public successMessage: string;

	// data gotten from the server that can be updated
	public eventData: Event;
	// data that will be sent to be updated
	public data: { [k: string]: any } = {};

	// Control if changes were made
	public changedValue = {
		name: false,
		description: false,
		prerequisites: false,
		address: false,
		startDate: false,
		endDate: false,
		startTime: false,
		endTime: false,
		imageURL: false,
	};

	constructor(
		private eventService: EventService,
		private cookieService: CookieService,
		private fb: FormBuilder,
		private router: Router,
	) {
		this.updateEvent = this.fb.group({
			name: ['', Validators.compose([Validators.required])],
			description: ['', Validators.compose([Validators.required])],
			prerequisites: ['', Validators.compose([Validators.required])],
			address: ['', Validators.compose([Validators.required])],
			startDate: ['', Validators.compose([Validators.required])],
			endDate: ['', Validators.compose([Validators.required])],
			startTime: ['', Validators.compose([Validators.required])],
			endTime: ['', Validators.compose([Validators.required])],
			imageURL: ['', Validators.compose([Validators.required])],
		});
	}

	ngOnInit() {
		const token: any = JwtHelper.decodeToken(this.cookieService.get('loginTokenGGJ'));
		if (token.userId === undefined) {
			this.router.navigateByUrl('/login').then();
		}
		if (token.userId !== undefined) {
			if (token.role !== 'admin' && token.role !== 'event manager') {
				this.router.navigateByUrl('/').then();
			}
			this.role = token.role;
			this.subscription = this.eventService.getEvents().subscribe(
				data => this.setDataIntoSearch(data),
				error => console.log(error),
			);
		}
	}

	private setDataIntoSearch(data): void {
		// Taken from: https://stackblitz.com/angular/njnvoyxdgrl?file=src%2Fapp%2Fautocomplete-overview-example.ts
		this.events = <Event[]>data;
		this.filteredEvents = this.eventCtrl.valueChanges.pipe(
			startWith(''),
			map(event => (event ? this._filterEvents(event) : this.events.slice())),
		);
	}

	private _filterEvents(value: string): Event[] {
		const filterValue = value;
		return this.events.filter(event => event.name.indexOf(filterValue) === 0);
	}

	// set the data in form inputs
	private setData(obj: Event): void {
		this.eventData = obj;
		this.updateEvent.controls['name'].setValue(this.eventData.name);
		this.updateEvent.controls['description'].setValue(this.eventData.description);
		this.updateEvent.controls['prerequisites'].setValue(this.eventData.prerequisites);
		this.updateEvent.controls['address'].setValue(this.eventData.address);
		this.updateEvent.controls['startDate'].setValue(this.eventData.startDate);
		this.updateEvent.controls['endDate'].setValue(this.eventData.endDate);

		const startTime = new Date(this.eventData.startDate).toLocaleTimeString('en-GB', {
			timeZone: 'UTC',
			hour: '2-digit',
			minute: '2-digit',
		});

		const endTime = new Date(this.eventData.endDate).toLocaleTimeString('en-GB', {
			timeZone: 'UTC',
			hour: '2-digit',
			minute: '2-digit',
		});
		this.updateEvent.controls['startTime'].setValue(startTime);
		this.updateEvent.controls['endTime'].setValue(endTime);
		this.updateEvent.controls['imageURL'].setValue(this.eventData.imageURL);
	}

	// Control if changes were made in the form
	public changesMade(): boolean {
		return !(
			!this.changedValue.name &&
			!this.changedValue.description &&
			!this.changedValue.prerequisites &&
			!this.changedValue.address &&
			!this.changedValue.startDate &&
			!this.changedValue.startTime &&
			!this.changedValue.endDate &&
			!this.changedValue.endTime &&
			!this.changedValue.imageURL
		);
	}

	// update user profile data
	public update(): void {
		if (this.updateEvent.valid) {
			this.data.id = this.eventData.id;

			this.successMessage = '';
			this.errorMessage = '';
			// Controls if changes were made, if no nothing gets submitted
			if (!this.changesMade()) {
				this.errorMessage = 'No changes made!';
				return;
			}

			// Gets both end and start time of the event
			const startTime = this.updateEvent.get('startTime').value;
			const endTime = this.updateEvent.get('endTime').value;

			// converts the time into hours and minutes
			const startTimeHours = parseInt(startTime.slice(0, 2), 10);
			const startTimeMinutes = parseInt(startTime.slice(3, 5), 10);
			const endTimeHours = parseInt(endTime.slice(0, 2), 10);
			const endTimeMinutes = parseInt(endTime.slice(3, 5), 10);

			// creates the date of the start and end of the event with the time
			const endDate = new Date(this.updateEvent.get('endDate').value).setHours(endTimeHours, endTimeMinutes, 0, 0);
			const startDate = new Date(this.updateEvent.get('startDate').value).setHours(
				startTimeHours,
				startTimeMinutes,
				0,
				0,
			);

			if (endDate <= startDate) {
				this.errorMessage = 'Check your dates!';
				return;
			}

			if (this.changedValue.name) {
				this.data.name = this.updateEvent.get('name').value;
				this.changedValue.name  = false;
			}
			if (this.changedValue.description) {
				this.data.description = this.updateEvent.get('description').value;
				this.changedValue.description = false;
			}
			if (this.changedValue.prerequisites) {
				this.data.prerequisites = this.updateEvent.get('prerequisites').value;
				this.changedValue.prerequisites = false;

			}
			if (this.changedValue.address) {
				this.data.address = this.updateEvent.get('address').value;
				this.changedValue.address = false;
			}
			if (this.changedValue.startDate) {
				const startDateInForm = this.updateEvent.get('startDate').value;
				const formatted_startDate =
					startDateInForm.getFullYear() + '-' + (startDateInForm.getMonth() + 1) + '-' + startDateInForm.getDate();
				this.data.startDate = formatted_startDate;
				this.changedValue.startDate = false;
			}
			if (this.changedValue.endDate) {
				const endDateInForm = this.updateEvent.get('endDate').value;
				const formatted_endDate =
					endDateInForm.getFullYear() + '-' + (endDateInForm.getMonth() + 1) + '-' + endDateInForm.getDate();
				this.data.endDate = formatted_endDate;
				this.changedValue.endDate = false;
			}
			if (this.changedValue.endTime) {
				this.data.endTime = this.updateEvent.get('endTime').value;
				this.changedValue.endTime = false;
			}
			if (this.changedValue.startTime) {
				this.data.startTime = this.updateEvent.get('startTime').value;
				this.changedValue.startTime = false;
			}
			if (this.changedValue.imageURL) {
				this.data.imageURL = this.updateEvent.get('imageURL').value;
				this.changedValue.imageURL = false;
			}

			// sends updated data
			this.eventService.update(this.data).subscribe(
				data => (this.successMessage = 'Event Updated'),
				error => (this.errorMessage = 'Error occurred updating your event.'),
			);
		}
	}
}
