import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { UsersService } from '../users.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { JwtHelper } from '../jwtHelper';
import { map, startWith } from 'rxjs/internal/operators';

export interface User {
	id: number;
	emailAddress: string;
	firstName: string;
	lastName: string;
	password: string;
	salt: string;
	diet: string;
	allergies: string;
	skillSet: string;
	hardware: string;
	sponsorEmailAgreed: boolean;
	role: string;
	paid: boolean;
}
@Component({
	selector: 'app-manage-event-payement',
	templateUrl: './manage-event-payement.component.html',
	styleUrls: ['./manage-event-payement.component.scss'],
})
export class ManageEventPayementComponent implements OnInit {
	public userCtrl = new FormControl('', Validators.required);
	public filteredUsers: Observable<User[]>;
	public users: User[];
	public role: string;

	// formgroup to control the input fields
	public updatePayement: FormGroup;
	// Subscription (to hand async data)
	private subscription: Subscription;
	// error message that will be displayed if an error happens
	public errorMessage: string;
	public successMessage: string;

	// data gotten from the server that can be updated
	public userData: User;
	// data that will be sent to be updated
	public data: { [k: string]: any } = {};

	// Control if changes were made
	public changedValue = {
		paid: false,
	};

	constructor(
		private userService: UsersService,
		private cookieService: CookieService,
		private fb: FormBuilder,
		private router: Router,
	) {
		this.updatePayement = this.fb.group({
			paid: [''],
		});
	}

	ngOnInit() {
		const token: any = JwtHelper.decodeToken(this.cookieService.get('loginTokenGGJ'));
		if (token.userId === undefined) {
			this.router.navigateByUrl('/login').then();
		} else {
			if (token.role !== 'accountant') {
				this.router.navigateByUrl('/').then();
			}
			this.role = token.role;
			this.subscription = this.userService.getAll().subscribe(
				data => this.setDataIntoSearch(data),
				error => console.log(error),
			);
		}
	}

	private setDataIntoSearch(data): void {
		// Taken from: https://stackblitz.com/angular/njnvoyxdgrl?file=src%2Fapp%2Fautocomplete-overview-example.ts
		this.users = <User[]>data;
		this.filteredUsers = this.userCtrl.valueChanges.pipe(
			startWith(''),
			map(user => (user ? this._filterUsers(user) : this.users.slice())),
		);
	}

	private _filterUsers(value: string): User[] {
		const filterValue = value;
		return this.users.filter(user => user.firstName.indexOf(filterValue) === 0);
	}

	// set the data in form inputs
	private setData(obj: User): void {
		this.userData = obj;
		this.updatePayement.controls['paid'].setValue(this.userData.paid);
	}

	// Control if changes were made in the form
	public changesMade(): boolean {
		return !!this.changedValue.paid;
	}

	// update user profile data
	public update(): void {
		if (this.updatePayement.valid) {
			this.data.id = this.userData.id;
			this.successMessage = '';
			this.errorMessage = '';

			// Controls if changes were made, if no nothing gets submitted
			if (!this.changesMade()) {
				this.errorMessage = 'No changes made!';
				return;
			}

			if (this.changedValue.paid) {
				if (this.updatePayement.get('paid').value === true) {
					this.data.paid = 'true';
				} else {
					this.data.paid = 'false';
				}
			}

			// sends updated data
			this.userService.userPayementUpdate(this.data).subscribe(
				data => (this.successMessage = 'Payment status updated!'),
				error => (this.errorMessage = 'Error occurred updating payment status.'),
			);
			this.changedValue.paid = false;
		}
	}
}
