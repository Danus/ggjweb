import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageEventPayementComponent } from './manage-event-payement.component';

describe('ManageEventPayementComponent', () => {
	let component: ManageEventPayementComponent;
	let fixture: ComponentFixture<ManageEventPayementComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ManageEventPayementComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ManageEventPayementComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
