import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { JwtHelper } from '../jwtHelper';

@Component({
	selector: 'app-group-main',
	templateUrl: './group-main.component.html',
	styleUrls: ['./group-main.component.scss'],
})
export class GroupMainComponent implements OnInit {
	public myGroupClass: string;
	public joinGroupClass: string;
	public newGroupClass: string;
	public allGroupClass: string;
	public groupNav: string;
	public groupNavAdmin: string;

	constructor(private router: Router, private cookieService: CookieService) {}

	private initGroupNav(token: any) {
		if (token.role === 'user') {
			this.groupNav = 'groupNav';
			this.groupNavAdmin = 'groupNavHide';

			this.myGroupClass = '';
			this.joinGroupClass = '';
			this.newGroupClass = '';

			if (this.router.url.includes('/groups')) {
				this.myGroupClass = 'active';
			} else if (this.router.url.includes('/joinGroup')) {
				this.joinGroupClass = 'active';
			} else if (this.router.url.includes('/newGroup')) {
				this.newGroupClass = 'active';
			}
		} else if (token.role === 'admin' || token.role === 'event manager') {
			this.groupNav = 'groupNavHide';
			this.groupNavAdmin = 'groupNav';

			this.newGroupClass = '';
			this.allGroupClass = '';

			if (this.router.url.includes('/newGroup')) {
				this.newGroupClass = 'active';
			} else if (this.router.url.includes('/allGroup')) {
				this.allGroupClass = 'active';
			}
		}
	}

	ngOnInit() {
		const token = JwtHelper.decodeToken(this.cookieService.get('loginTokenGGJ'));
		this.initGroupNav(token);
	}
}
