import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-event-main',
  templateUrl: './event-main.component.html',
  styleUrls: ['./event-main.component.scss']
})
export class EventMainComponent implements OnInit {

  public createEvent: string;
  public manageEvent: string;

  constructor(private router: Router) { }

  ngOnInit() {
    this.createEvent = '';
    this.manageEvent = '';

    if (this.router.url.includes('/createEvent')) {
      this.createEvent = 'active';
    } else if (this.router.url.includes('/manageEvent')) {
      this.manageEvent = 'active';
    }
  }

}
