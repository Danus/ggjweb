import { Component, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { CdkTableModule } from '@angular/cdk/table';
import { GroupService } from '../group.service';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material';
import { DialogPasswordComponent } from '../dialog-password/dialog-password.component';
import { CookieService } from 'ngx-cookie-service';
import { JwtHelper } from '../jwtHelper';
import { DialogMessageComponent } from '../dialog-message/dialog-message.component';
import { Router } from '@angular/router';

export interface Groups {
	NumberOfMembers: number;
	Members: string;
	Position: number;
	GroupName: string;
	EventName: string;
	Description: string;
	Status: any;
	GroupId: number;
	Leader: any;
}

@Component({
	selector: 'app-join-group',
	templateUrl: './join-group.component.html',
	styleUrls: ['./join-group.component.scss'],
	animations: [
		trigger('detailExpand', [
			state('collapsed', style({ height: '0px', minHeight: '0' })),
			state('expanded', style({ height: '*' })),
			transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
		]),
	],
})
export class JoinGroupComponent implements OnInit {
	public currentUserId: number;
	password: string;
	groupName: string;
	public done = false;
	public groups: Groups[] = [];
	// Columns to Display
	public columnsToDisplay = ['EventName', 'GroupName', 'NumberOfMembers', 'Status'];
	//  public dataSource = this.project;
	public dataSource = this.groups;
	// Columns to Display if Expanded
	public expandedElement: Groups | null;
	// Subscription (to hand async data)
	private subscription: Subscription;

	constructor(
		private groupService: GroupService,
		public dialog: MatDialog,
		private cookieService: CookieService,
		private router: Router,
	) {}

	// Show OR Hide STATUS-Icon
	public showIcon(value: any): string {
		if (value.name === 'Open') {
			return 'show';
		} else {
			return 'hide';
		}
	}

	// Select STATUS-Icon
	public selectIcon(value: any): string {
		if (value.name === 'Open') {
			return value.lock;
		}
	}

	// Table header
	public toTitle(title: string): string {
		return title.replace(/([A-Z])/g, ' $1');
	}

	// Check if Group is OPEN or CLOSED
	public checkObject(value: any): string {
		if (value.name === 'Open' || value.name === 'Closed') {
			return value.name;
		} else {
			return value;
		}
	}

	ngOnInit() {
		const token: any = JwtHelper.decodeToken(this.cookieService.get('loginTokenGGJ'));
		if (token.userId === undefined) {
			this.router.navigateByUrl('/').then();
		} else {
			if (token.role === 'admin' || token.role === 'event manager' || token.role === 'accountant') {
				this.router.navigateByUrl('/').then();
			}
			this.currentUserId = token.userId;
			this.subscription = this.groupService.getGroups().subscribe(
				data => this.setData(data),
				error => console.log(error),
			);
		}
	}

	private setData(data: any): void {
		let num = 1;
		for (const group of data) {

			const todaysDate = new Date();
			const eventEndDate = new Date(group.event.endDate);
			if (eventEndDate < todaysDate) {
				continue;
			}
			let userInGroup = false;
			for (let z = 0; z < group.users.length; z++) {
				if (group.users[z].id === this.currentUserId) {
					userInGroup = true;
					break;
				}
			}
			if (group.leader.id !== this.currentUserId && userInGroup !== true) {
				let status: any;
				if (group.status === 'closed') {
					status = { name: 'Open', lock: 'lock' };
				}
				if (group.openStatus === 'open') {
					status = { name: 'Open', lock: 'lock_open' };
				} else {
					status = { name: 'Open', lock: 'lock' };
				}

				// Number of Members
				const numM = group.users.length + 1;

				let usersNames = '';
				if (group.users.length > 0) {
					for (let i = 0; i < group.users.length; i++) {
						if (i === group.users.length - 1) {
							usersNames = usersNames + group.users[i].firstName + ' ' + group.users[i].lastName;
						} else {
							usersNames = usersNames + group.users[i].firstName + ' ' + group.users[i].lastName + ', ';
						}
					}
				}
				let description = '';
				if (group.description !== null) {
					description = group.description;
				}

				const leaderName = group.leader.firstName + ' ' + group.leader.lastName;
				const groupToAdd = {
					Position: num,
					GroupName: group.name,
					EventName: group.event.name,
					Description: description,
					Status: status,
					NumberOfMembers: numM,
					Members: usersNames,
					Leader: leaderName,
					GroupId: group.id,
				};
				this.groups.push(groupToAdd);
				num++;
			}
		}
		this.done = true;
	}

	public joinGroup(group: any) {
		if (group.Status.lock === 'lock_open') {
			const information = { id: group.GroupId, password: '' };
			this.sendDataToServer(information);
		} else {
			this.groupName = group.GroupName;
			const dialogRef = this.dialog.open(DialogPasswordComponent, {
				width: '250px',
				data: { groupName: this.groupName, password: this.password },
			});

			dialogRef.afterClosed().subscribe(result => {
				if (result !== undefined) {
					this.password = result;
					const information = { id: group.GroupId, password: this.password };
					this.sendDataToServer(information);
				}
			});
		}
	}

	public openMessage(datatosend: any) {
		let information = '';
		if (datatosend.success === 'false') {
			information = 'Wrong Password!';
		}
		if (datatosend.success === 'true') {
			information = 'Group joined!';
		}
		const dialogRef = this.dialog.open(DialogMessageComponent, {
			width: '250px',
			data: { message: information },
		});

		dialogRef.afterClosed().subscribe(result => {
			location.reload();
		});
	}

	private sendDataToServer(dataToSend: any) {
		const information = { id: dataToSend.id, password: dataToSend.password };
		this.subscription = this.groupService.addUserGroup(information).subscribe(
			data => this.openMessage(data),
			error => console.log(error),
		);
	}
}
