import { Component, OnInit } from '@angular/core';
import { RegisterService } from '../register.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { JwtHelper } from '../jwtHelper';
import { CookieService } from 'ngx-cookie-service';

@Component({
	selector: 'app-register-form',
	templateUrl: './register-form.component.html',
	styleUrls: ['./register-form.component.scss'],
})
export class RegisterFormComponent implements OnInit {
	constructor(private registerService: RegisterService, private router: Router, private cookieService: CookieService) {}

	public registerCred = new FormGroup({
		firstName: new FormControl('', Validators.required),
		lastName: new FormControl('', Validators.required),
		emailAddress: new FormControl('', [Validators.required, Validators.email]),
		password: new FormControl('', [Validators.required, Validators.minLength(8)]),
		hardware: new FormControl('', Validators.required),
		allergies: new FormControl(''),
		skillSet: new FormControl(''),
		diet: new FormControl('', Validators.required),
		sponsorEmailAgreed: new FormControl(false),
	});

	public optionsEat = ['None', 'Vegan', 'Vegetarian'];

	public errorMessage: string;

	private registrationSuccess(): void {
		this.router.navigateByUrl('/login').then();
	}

	private registrationError(error: any): void {
		this.errorMessage = 'This Email has already been taken!';
	}

	ngOnInit() {
		const token: any = JwtHelper.decodeToken(this.cookieService.get('loginTokenGGJ'));
		if (token.userId !== undefined) {
			this.router.navigateByUrl('/').then();
		}
	}

	public register(): void {
		if (this.registerCred.valid) {
			this.registerService.register(this.registerCred).subscribe(
				data => this.registrationSuccess(),
				error => this.registrationError(error),
			);
		} else {
			this.errorMessage = 'Please fill all the required fields.';
		}
	}
}
