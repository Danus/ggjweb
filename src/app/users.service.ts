import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
	providedIn: 'root',
})
export class UsersService {
	_url: string;
	constructor(private _http: HttpClient, private cookieService: CookieService) {}

	private httpOptions = {
		headers: new HttpHeaders({
			'Accept': 'application/json',
			'Content-Type': 'application/json',
		}),
	};

	public userPayementUpdate(data: any) {
		this._url = 'user/paid';
		return this._http.patch(this._url, data, this.httpOptions);
	}

	public update(data: any) {
		this._url = '/user/update';
		return this._http.patch(this._url, data, this.httpOptions);
	}

	public get(id: number) {
		this._url = '/user/' + id;
		return this._http.get(this._url, this.httpOptions);
	}

	public getAll() {
		this._url = '/user/';
		return this._http.get(this._url, this.httpOptions);
	}

	public getUserNoGroup() {
		this._url = '/user/noGroup';
		return this._http.get(this._url, this.httpOptions);
	}

	public isAuthenticated(): boolean {
		return this.cookieService.get('loginTokenGGJ') !== null;
	}

	public getAuthToken(): string {
		return this.cookieService.get('loginTokenGGJ');
	}
}
