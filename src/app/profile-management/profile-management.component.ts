import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Subscription } from 'rxjs';
import { CustomValidators } from './custom-validators';
import { JwtHelper } from '../jwtHelper';
import { CookieService } from 'ngx-cookie-service';
import * as JWT from 'jwt-decode';
import { Router } from '@angular/router';
@Component({
	selector: 'app-profile-management',
	templateUrl: './profile-management.component.html',
	styleUrls: ['./profile-management.component.scss'],
})
export class ProfileManagementComponent implements OnInit {
	// formgroup to control the input fields
	public updateProfile: FormGroup;
	// Subscription (to hand async data)
	private subscription: Subscription;
	// error message that will be displayed if an error happens
	public errorMessage: string;
	public successMessage: string;

	// data gotten from the server that can be updated
	public userData: any;
	// data that will be sent to be updated
	public data: { [k: string]: any } = {};

	// controls page is done loading
	public done = false;

	private token: any;

	// Option a user has to 'eat'
	public optionsEat = ['None', 'Vegan', 'Vegetarian'];
	// Control if changes were made
	public changedValue = {
		hardware: false,
		allergies: false,
		skillSet: false,
		diet: false,
	};

	constructor(
		private userService: UsersService,
		private cookieService: CookieService,
		private fb: FormBuilder,
		private router: Router,
	) {
		this.updateProfile = this.fb.group(
			{
				diet: ['', Validators.compose([Validators.required])],
				skillSet: [''],
				allergies: [''],
				hardware: ['', Validators.compose([Validators.required])],
				password: [
					'',
					Validators.compose([
						// check whether the entered password has a 8 characters
						Validators.minLength(8),
					]),
				],
				confirmPassword: [''],
			},
			{
				// check whether our password and confirm password match
				validator: CustomValidators.passwordMatchValidator,
			},
		);
	}

	ngOnInit() {
		const token: any = JwtHelper.decodeToken(this.cookieService.get('loginTokenGGJ'));
		this.token = token;
		if (token.userId === undefined) {
			this.router.navigateByUrl('/login').then();
		} else {
			this.subscription = this.userService.get(token.userId).subscribe(
				data => this.setData(data),
				error => console.log(error),
			);
		}
	}

	// set the data in ngOnInit()
	private setData(obj: any): void {
		if (this.token.userId !== obj.id) {
			this.router.navigateByUrl('/').then();
		}
		this.userData = obj;
		this.updateProfile.controls['hardware'].setValue(this.userData.hardware);
		this.updateProfile.controls['skillSet'].setValue(this.userData.skillSet);
		this.updateProfile.controls['allergies'].setValue(this.userData.allergies);
		this.updateProfile.controls['diet'].setValue(this.userData.diet);
		this.done = true;
	}

	// Control if changes were made in the form
	public changesMade(): boolean {
		return !(
			!this.changedValue.hardware &&
			!this.changedValue.allergies &&
			!this.changedValue.diet &&
			!this.changedValue.skillSet &&
			this.updateProfile.get('password').value === ''
		);
	}

	// update user profile data
	public update(): void {
		if (this.updateProfile.valid) {
			this.successMessage = '';
			this.errorMessage = '';
			// Controls if changes were made, if no nothing gets submitted
			if (!this.changesMade()) {
				this.errorMessage = 'No changes made!';
				return;
			}
			this.data.id = this.userData.id;
			if (this.updateProfile.get('password').value !== '') {
				this.data.password = this.updateProfile.get('password').value;
				this.updateProfile.controls['password'].setValue('');
				this.updateProfile.controls['confirmPassword'].setValue('');
			}
			if (this.changedValue.hardware) {
				this.data.hardware = this.updateProfile.get('hardware').value;
				this.changedValue.hardware = false;
			}
			if (this.changedValue.allergies) {
				this.data.allergies = this.updateProfile.get('allergies').value;
				this.changedValue.allergies = false;
			}
			if (this.changedValue.skillSet) {
				this.data.skillSet = this.updateProfile.get('skillSet').value;
				this.changedValue.skillSet = false;
			}
			if (this.changedValue.diet) {
				this.data.diet = this.updateProfile.get('diet').value;
				this.changedValue.diet = false;
			}

			// sends updated data
			this.userService.update(this.data).subscribe(
				data => (this.successMessage = 'Profile updated.'),
				error => (this.errorMessage = 'Error occurred updating your profile.'),
			);
		}
	}
}
