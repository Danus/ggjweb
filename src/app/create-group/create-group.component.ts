import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { UsersService } from '../users.service';
import { GroupService } from '../group.service';
import { CookieService } from 'ngx-cookie-service';
import { JwtHelper } from '../jwtHelper';
import { map, startWith } from 'rxjs/internal/operators';
import { Router } from '@angular/router';

export interface User {
	id: number;
	emailAddress: string;
	firstName: string;
	lastName: string;
}

export interface Event {
	id: number;
	name: string;
}

@Component({
	selector: 'app-create-group',
	templateUrl: './create-group.component.html',
	styleUrls: ['./create-group.component.scss'],
})
export class CreateGroupComponent implements OnInit {
	private dataToSend: { [k: string]: any } = {};
	public errorMessage: string;
	public successMessage: string;
	public token: any;
	public leaderCtrl = new FormControl('', Validators.required);
	public filteredUsers: Observable<User[]>;
	public users: User[];
	// Subscription (to hand async data)
	private subscription: Subscription;
	public events = [];
	private idLeader = 0;

	constructor(
		private userService: UsersService,
		private groupService: GroupService,
		private cookieService: CookieService,
		private router: Router,
	) {}

	public createGroup: FormGroup = new FormGroup({
		name: new FormControl('', [Validators.required, Validators.maxLength(55)]),
		password: new FormControl('', Validators.minLength(8)),
		description: new FormControl('', Validators.maxLength(255)),
		event: new FormControl('', [Validators.required]),
		leader: this.leaderCtrl,
	});

	ngOnInit() {
		this.token = JwtHelper.decodeToken(this.cookieService.get('loginTokenGGJ'));
		if (this.token.userId === undefined) {
			this.router.navigateByUrl('/login').then();
		}
		if (this.token.userId !== undefined) {
			if (this.token.role === 'accountant') {
				this.router.navigateByUrl('/').then();
			}
			this.subscription = this.groupService.getEvents().subscribe(
				data => this.setEvents(data),
				error => console.log(error),
			);

			if (this.token.role === 'admin' || this.token.role === 'event manager') {
				this.subscription = this.userService.getUserNoGroup().subscribe(
					data => this.setData(data),
					error => console.log(error),
				);
			} else {
				this.leaderCtrl.setValue(this.token.userId);
			}
		}
	}

	private setData(data): void {
		// Taken from: https://stackblitz.com/angular/njnvoyxdgrl?file=src%2Fapp%2Fautocomplete-overview-example.ts
		this.users = <User[]>data;
		this.filteredUsers = this.leaderCtrl.valueChanges.pipe(
			startWith(''),
			map(user => (user ? this._filterUsers(user) : this.users.slice())),
		);
	}

	public setEvents(data): void {
		for (const event of data) {
			const todaysDate = new Date();
			const eventEndDate = new Date(event.endDate);
			if (eventEndDate < todaysDate) {
				continue;
			}
			const eventToAdd = { id: event.id, name: event.name };
			this.events.push(eventToAdd);
		}
	}

	private _filterUsers(value: string): User[] {
		const filterValue = value;
		return this.users.filter(user => user.firstName.indexOf(filterValue) === 0);
	}

	public create(): void {
		if (this.createGroup.valid) {
			this.errorMessage = '';
			this.successMessage = '';
			this.dataToSend.name = this.createGroup.get('name').value;
			this.dataToSend.eventId = this.createGroup.get('event').value;
			if (this.idLeader !== 0) {
				this.dataToSend.leaderId = this.idLeader;
			} else {
				this.dataToSend.leaderId = this.createGroup.get('leader').value;
			}

			if (this.createGroup.get('password').value !== '') {
				this.dataToSend.password = this.createGroup.get('password').value;
			}

			if (this.createGroup.get('description').value !== '') {
				this.dataToSend.description = this.createGroup.get('description').value;
			}
			// sends updated data
			this.groupService.createGroup(this.dataToSend).subscribe(
				data => this.setMessage(data),
				error => (this.errorMessage = error.error.message),
			);
		}
	}


	private setMessage(message: any) {
		if (message.success === 'This name is already taken!') {
			this.errorMessage = message.success;
		} else if (message.success === 'Group created successfully!') {
			this.router.navigateByUrl('/groups').then();
		} else {
			this.errorMessage = message.success;
		}
	}

	public setLeaderId(id: number) {
		this.idLeader = id;
	}
}
