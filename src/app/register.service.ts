import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {FormGroup} from '@angular/forms';

@Injectable({
	providedIn: 'root'
})
export class RegisterService {

	_url = '/auth/register';
	constructor(private _http: HttpClient) { }

	private httpOptions = {
		headers: new HttpHeaders({
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		})
	};
	public register(regCred: FormGroup) {
		return this._http.post<FormGroup>(this._url, regCred.value, this.httpOptions);
	}

}
