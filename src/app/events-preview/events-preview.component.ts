import { Component, OnInit } from '@angular/core';
import { EventService } from '../event.service';

@Component({
	selector: 'app-events-preview',
	templateUrl: './events-preview.component.html',
	styleUrls: ['./events-preview.component.scss'],
})
export class EventsPreviewComponent implements OnInit {
	public events: any[];
	constructor(private eventService: EventService) {}
	ngOnInit() {
		this.events = [];
		this.eventService.getEventsForPreview().subscribe(
			data => this.setData(data),
			error => console.log(error),
		);
	}

	private setData(data: any): void {
		if (data) {
			this.events = data;
		}
	}
}
