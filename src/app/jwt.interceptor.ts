import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import {UsersService} from './users.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
	constructor(private usersService: UsersService) {}

	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		// add authorization header with jwt token if available
		if (this.usersService.isAuthenticated()) {
			const token = this.usersService.getAuthToken();
			request = request.clone({
				setHeaders: {
					Authorization: `Bearer ${token}`,
				},
			});
		}

		return next.handle(request);
	}
}
