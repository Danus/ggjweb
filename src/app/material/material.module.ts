import { NgModule } from '@angular/core';

import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatNativeDateModule} from '@angular/material/core';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {FormsModule} from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatGridListModule } from '@angular/material/grid-list';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {
	MatAutocompleteModule,
	MatDialogModule,
	MatMenuModule,
	MatProgressSpinnerModule,
	MatSelectModule,
	MatTableModule
} from '@angular/material';

const MaterialComponents = [
	MatButtonModule,
	LayoutModule,
	MatToolbarModule,
	MatSidenavModule,
	MatIconModule,
	MatListModule,
	MatNativeDateModule,
	MatIconModule,
	MatButtonModule,
	MatCheckboxModule,
	MatToolbarModule,
	FormsModule,
	MatCardModule,
	MatFormFieldModule,
	MatInputModule,
	MatListModule,
	MatRadioModule,
	MatDatepickerModule,
	MatGridListModule,
	MatMenuModule,
	MatAutocompleteModule,
	MatTableModule,
	MatSelectModule,
	MatProgressSpinnerModule,
	MatDialogModule,
	NgxMaterialTimepickerModule
];
@NgModule({

	imports: [MaterialComponents],
	exports: [MaterialComponents]
})
export class MaterialModule { }
