import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class LoginService {

	constructor(private http: HttpClient) {}

	private loginUrl = '/auth/login';

	private httpOptions = {
		headers: new HttpHeaders({
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		})
	};

	public login(email: string, password: string) {
		const loginCredentials: object = {
			'emailAddress': email,
			'password': password
		};
		return this.http.post<object>(this.loginUrl, loginCredentials, this.httpOptions);
	}
}
