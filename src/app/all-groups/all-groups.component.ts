import { Component, OnInit } from '@angular/core';
import { GroupService } from '../group.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JwtHelper } from '../jwtHelper';
import { animate, state, style, transition, trigger } from '@angular/animations';

export interface Groups {
	NumberOfMembers: number;
	Members: string;
	Position: number;
	GroupName: string;
	EventName: string;
	Description: string;
	Status: any;
	GroupId: number;
	Leader: any;
	URL: string;
}

@Component({
	selector: 'app-all-groups',
	templateUrl: './all-groups.component.html',
	styleUrls: ['./all-groups.component.scss'],
	animations: [
		trigger('detailExpand', [
			state('collapsed', style({ height: '0px', minHeight: '0' })),
			state('expanded', style({ height: '*' })),
			transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
		]),
	],
})
export class AllGroupsComponent implements OnInit {
	public currentUserId: number;
	public done = false;
	public groups: Groups[] = [];
	// Columns to Display
	public columnsToDisplay = ['EventName', 'GroupName', 'NumberOfMembers', 'Status'];
	//  public dataSource = this.project;
	public dataSource = this.groups;
	// Columns to Display if Expanded
	public expandedElement: Groups | null;
	// Subscription (to hand async data)
	private subscription: Subscription;
	private token: any;
	constructor(private groupService: GroupService, private cookieService: CookieService, private router: Router) {}

	ngOnInit() {
		const token: any = JwtHelper.decodeToken(this.cookieService.get('loginTokenGGJ'));
		this.token = token;
		if (token.userId === undefined) {
			this.router.navigateByUrl('/login').then();
		} else {
			if (token.role !== 'admin' && token.role !== 'event manager') {
				this.router.navigateByUrl('/').then();
			}
			this.currentUserId = token.userId;
			this.subscription = this.groupService.getGroups().subscribe(
				data => this.setData(data),
				error => console.log(error),
			);
		}
	}

	private setData(data: any): void {
		let num = 1;
		for (const group of data) {
			console.log(num);
			let status: any;
			if (group.openStatus === 'open') {
				status = { name: 'Open', lock: 'lock_open' };
			} else if (group.openStatus === 'closed') {
				status = { name: 'Closed', lock: 'lock' };
			} else {
				status = { name: 'Open', lock: 'lock' };
			}

			// Number of Members
			const numM = group.users.length + 1;

			let usersNames = '';
			if (group.users.length > 0) {
				for (let i = 0; i < group.users.length; i++) {
					if (i === group.users.length - 1) {
						usersNames = usersNames + group.users[i].firstName + ' ' + group.users[i].lastName;
					} else {
						usersNames = usersNames + group.users[i].firstName + ' ' + group.users[i].lastName + ', ';
					}
				}
			}
			let description = '';
			if (group.description !== null) {
				description = group.description;
			}

			const leaderName = group.leader.firstName + ' ' + group.leader.lastName;
			const groupToAdd = {
				Position: num,
				GroupName: group.name,
				EventName: group.event.name,
				Description: description,
				Status: status,
				NumberOfMembers: numM,
				Members: usersNames,
				Leader: leaderName,
				GroupId: group.id,
				URL: group.url,
			};
			this.groups.push(groupToAdd);
			num++;
		}
		this.done = true;
	}

	// Show OR Hide STATUS-Icon
	public showIcon(value: any): string {
		if (value.name === 'Open' || value.name === 'Closed') {
			return 'show';
		} else {
			return 'hide';
		}
	}

	// Select STATUS-Icon
	public selectIcon(value: any): string {
		if (value.name === 'Closed') {
			return value.lock;
		}
		if (value.name === 'Open') {
			return value.lock;
		}
	}

	// Table header
	public toTitle(title: string): string {
		return title.replace(/([A-Z])/g, ' $1');
	}

	// Check if Group is OPEN or CLOSED
	public checkObject(value: any): string {
		if (value.name === 'Open' || value.name === 'Closed') {
			return value.name;
		} else {
			return value;
		}
	}
}
