import { Component, OnInit } from '@angular/core';
import { EventService } from '../event.service';

@Component({
	selector: 'app-events',
	templateUrl: './events.component.html',
	styleUrls: ['./events.component.scss'],
})
export class EventsComponent implements OnInit {
	constructor(private eventService: EventService) {}

	public events: any[];
	public noEvent: boolean;
	ngOnInit() {
		this.eventService.getEvents().subscribe(
			data => this.setData(data),
			error => console.log(error),
		);
	}

	private setData(data: any): void {
		if (data === null) {
			this.noEvent = true;
		} else {
			this.events = data;
		}
	}
}
