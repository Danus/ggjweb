import { Component, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Subscription } from 'rxjs';
import { JwtHelper } from '../jwtHelper';
import { GroupService } from '../group.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { DialogMessageComponent } from '../dialog-message/dialog-message.component';
import { MatDialog } from '@angular/material';

export interface Groups {
	NumberOfMembers: number;
	Members: string;
	Position: number;
	GroupName: string;
	EventName: string;
	Description: string;
	GroupId: number;
	URL: string;
	Leader: string;
	LeaderId: number;
}

@Component({
	selector: 'app-my-groups',
	templateUrl: './my-groups.component.html',
	styleUrls: ['./my-groups.component.scss'],
	animations: [
		trigger('detailExpand', [
			state('collapsed', style({ height: '0px', minHeight: '0' })),
			state('expanded', style({ height: '*' })),
			transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
		]),
	],
})
export class MyGroupsComponent implements OnInit {
	public token: any;
	public currentUserId: number;
	public done = false;
	public groups: Groups[] = [];
	// Columns to Display
	public columnsToDisplay = ['EventName', 'GroupName', 'NumberOfMembers'];
	// Datasource
	//  public dataSource = this.project;
	public dataSource = this.groups;
	// Columns to Display if Expanded
	public expandedElement: Groups | null;
	// Subscription (to hand async data)
	private subscription: Subscription;

	constructor(
		private groupService: GroupService,
		private cookieService: CookieService,
		private _router: Router,
		public dialog: MatDialog,
	) {}

	// Show OR Hide STATUS-Icon
	public showIcon(value: any): string {
		if (value.name === 'Open') {
			return 'show';
		} else {
			return 'hide';
		}
	}

	// Select STATUS-Icon
	public selectIcon(value: any): string {
		if (value.name === 'Open') {
			return value.lock;
		}
	}

	// Table header
	public toTitle(title: string): string {
		return title.replace(/([A-Z])/g, ' $1');
	}

	// Check if Group is OPEN or CLOSED
	public checkObject(value: any): string {
		if (value.name === 'Open' || value.name === 'Closed') {
			return value.name;
		} else {
			return value;
		}
	}

	public leaveGroup(id) {
		this.subscription = this.groupService.leaveGroup(id).subscribe(
			data => this.openMessage(data),
			error => this.openMessage('error!'),
		);
	}

	public manageGroup(group: Groups) {
		this._router
			.navigate(['/manageGroup', group.GroupId], {
				queryParams: { searchTerm: group.GroupId },
			})
			.then();
	}

	ngOnInit() {
		const token: any = JwtHelper.decodeToken(this.cookieService.get('loginTokenGGJ'));
		this.token = token;
		if (token.userId === undefined) {
			this._router.navigateByUrl('/login').then();
		} else {
			if (token.role === 'admin' || token.role === 'event manager' || token.role === 'accountant') {
				this._router.navigateByUrl('/').then();
			}
			this.currentUserId = token.userId;
			this.subscription = this.groupService.getGroupsOfUser(token.userId).subscribe(
				data => this.setData(data),
				error => console.log(error),
			);
		}
	}

	private setData(data: any): void {
		let num = 1;
		for (const group of data) {
			// Number of Members
			const numM = group.users.length + 1;
			let usersNames = '';
			if (group.users.length > 0) {
				for (let i = 0; i < group.users.length; i++) {
					if (i === group.users.length - 1) {
						usersNames = usersNames + group.users[i].firstName + ' ' + group.users[i].lastName;
					} else {
						usersNames = usersNames + group.users[i].firstName + ' ' + group.users[i].lastName + ', ';
					}
				}
			}

			const leaderName = group.leader.firstName + ' ' + group.leader.lastName;
			const groupToAdd = {
				Position: num,
				GroupName: group.name,
				EventName: group.event.name,
				Description: group.description,
				NumberOfMembers: numM,
				Members: usersNames,
				Leader: leaderName,
				GroupId: group.id,
				URL: group.url,
				LeaderId: group.leader.id,
			};
			this.groups.push(groupToAdd);
			num++;
		}
		this.done = true;
	}
	public openMessage(dataToSend: any) {
		let information = '';
		if (dataToSend.success === 'false') {
			information = 'Something went wrong in the server!';
		}
		if (dataToSend.success === 'true') {
			information = 'Group left!';
		}
		if (dataToSend === 'error') {
			information = 'Something went wrong in the server!';
		}
		const dialogRef = this.dialog.open(DialogMessageComponent, {
			width: '250px',
			data: { message: information },
		});

		dialogRef.afterClosed().subscribe(result => {
			location.reload();
		});
	}
}
