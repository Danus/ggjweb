import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
	providedIn: 'root',
})
export class EventService {
	_url: string;

	constructor(private _http: HttpClient, private cookieService: CookieService) {}

	private httpOptions = {
		headers: new HttpHeaders({
			'Accept': 'application/json',
			'Content-Type': 'application/json',
		}),
	};

	public getEvent(id: number) {
		this._url = '/event/' + id;
		return this._http.get(this._url, this.httpOptions);
	}
	public getEvents() {
		this._url = '/event';
		return this._http.get(this._url, this.httpOptions);
	}

	public getGroupByEvent(id: number) {
		this._url = '/event/group/' + id;
		return this._http.get(this._url, this.httpOptions);
	}

	public createEvent(data: any) {
		this._url = '/event';
		return this._http.post(this._url, data, this.httpOptions);
	}

	public update(data: any) {
		this._url = '/event/update';
		return this._http.patch(this._url, data, this.httpOptions);
	}

	public getEventsForPreview() {
		this._url = '/event/preview';
		return this._http.get(this._url, this.httpOptions);
	}
}
