import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { DragulaModule } from 'ng2-dragula';
import { BrowserModule } from '@angular/platform-browser';
import { MainComponent } from './main/main.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { RegisterFormComponent } from './register-form/register-form.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { EventsPreviewComponent } from './events-preview/events-preview.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ProfileManagementComponent } from './profile-management/profile-management.component';
import { CookieService } from 'ngx-cookie-service';
import { LogoutComponent } from './logout/logout.component';
import { JwtInterceptor } from './jwt.interceptor';
import { ProfileManagementAdminComponent } from './profile-management-admin/profile-management-admin.component';
import { CreateGroupComponent } from './create-group/create-group.component';
import { JoinGroupComponent } from './join-group/join-group.component';
import { MyGroupsComponent } from './my-groups/my-groups.component';
import { GroupManagementComponent } from './group-management/group-management.component';
import { GroupMainComponent } from './group-main/group-main.component';
import { DialogPasswordComponent } from './dialog-password/dialog-password.component';
import { DialogMessageComponent } from './dialog-message/dialog-message.component';
import { EventsComponent } from './events/events.component';
import { CreateEventComponent } from './create-event/create-event.component';
import { MAT_DATE_LOCALE } from '@angular/material';
import { AllGroupsComponent } from './all-groups/all-groups.component';
import { EventManagementComponent } from './event-management/event-management.component';
import { EventDetailComponent } from './event-detail/event-detail.component';
import { ManageEventPayementComponent } from './manage-event-payement/manage-event-payement.component';
import { EventMainComponent } from './event-main/event-main.component';
import { ContactComponent } from './contact/contact.component';

// For AoT compilation:
export function getWindow() {
	return window;
}

@NgModule({
	declarations: [
		AppComponent,
		MainComponent,
		RegisterFormComponent,
		LoginFormComponent,
		LogoutComponent,
		EventsComponent,
		EventsPreviewComponent,
		ProfileManagementComponent,
		CreateGroupComponent,
		JoinGroupComponent,
		ProfileManagementAdminComponent,
		MyGroupsComponent,
		GroupManagementComponent,
		GroupMainComponent,
		CreateGroupComponent,
		DialogPasswordComponent,
		DialogMessageComponent,
		CreateEventComponent,
		AllGroupsComponent,
		EventManagementComponent,
		EventDetailComponent,
		ManageEventPayementComponent,
		EventMainComponent,
		ContactComponent,
	],
	imports: [
		HttpClientModule,
		DragulaModule.forRoot(),
		// Add .withServerTransition() to support Universal rendering.
		// The application ID can be any identifier which is unique on
		// the page.
		BrowserModule.withServerTransition({ appId: 'my-app' }),
		TransferHttpCacheModule,
		MaterialModule,
		SharedModule,
		ReactiveFormsModule,
		RouterModule.forRoot([
			{
				path: '',
				component: EventsPreviewComponent,
			},
			{
				path: 'allEvents',
				component: EventsComponent,
			},
			{
				path: 'eventDetail/:id',
				component: EventDetailComponent,
			},
			{
				path: 'register',
				component: RegisterFormComponent,
			},
			{
				path: 'login',
				component: LoginFormComponent,
			},
			{
				path: 'logout',
				component: LogoutComponent,
			},
			{
				path: 'updateProfile',
				component: ProfileManagementComponent,
			},
			{
				path: 'updateProfileAdmin',
				component: ProfileManagementAdminComponent,
			},
			{
				path: 'managePayment',
				component: ManageEventPayementComponent,
			},
			{
				path: 'newGroup',
				component: CreateGroupComponent,
			},
			{
				path: 'joinGroup',
				component: JoinGroupComponent,
			},
			{
				path: 'manageGroup/:id',
				component: GroupManagementComponent,
			},
			{
				path: 'groups',
				component: MyGroupsComponent,
			},
			{
				path: 'createEvent',
				component: CreateEventComponent,
			},
			{
				path: 'allGroups',
				component: AllGroupsComponent,
			},
			{
				path: 'manageEvent',
				component: EventManagementComponent,
			},
			{
				path: '**',
				redirectTo: '',
			},
		]),
		BrowserAnimationsModule,
	],
	entryComponents: [JoinGroupComponent, DialogPasswordComponent, DialogMessageComponent, MyGroupsComponent],
	providers: [
		CookieService,
		{ provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
		{ provide: MAT_DATE_LOCALE, useValue: 'en-GB' },
	],
	bootstrap: [AppComponent],
})
export class AppModule {}
